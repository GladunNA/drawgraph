﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CourseWork3_Graph
{
    class Vertex  //вершина
    {
        public int x, y;
        public string num;
        public List<int> contacts;
        public Brush color;

        public Vertex(int x, int y, int num, int M, int LeftC, int UpC)
        {
            this.x = (x + LeftC) * M;
            this.y = (y + UpC) * M;
            string St = "";
            int k = 0;
            num += 1;
            {
                while (num > 0)
                {
                    k = num % 26;
                    St = ((char)('A' + k - 1)).ToString() + St;
                    num = num / 26;
                }
            }
            this.num = St;
            contacts = new List<int>();
            color = Brushes.White;
        }
    }

    class Edge  //ребро
    {
        public int v1, v2;
        public bool Orient;
        public string num;
        public int w;

        public Edge(int v1, int v2, bool Orient, int num)
        {
            this.v1 = v1;
            this.v2 = v2;
            this.Orient = Orient;
            string St = "";
            int k = 0;
            num += 1;
            {
                while (num > 0)
                {
                    k = num % 26;
                    St = ((char)('a' + k - 1)).ToString() + St;
                    num = num / 26;
                }
            }
            this.num = St;
            this.w = 1;
        }
    }

    class DrawGraph
    {
        Bitmap bitmap; //параметры графики
        Graphics gr;   //
        Pen blackPen;
        Pen redPen;
        Pen darkGoldPen;
        Font fo;    //шрифт
        Brush br;   //кисть
        PointF point;  //точка
        public int R = 20; //радиус окружности вершины

        public DrawGraph(int width, int height)
        {
            bitmap = new Bitmap(width, height);
            gr = Graphics.FromImage(bitmap);
            clearSheet();
            blackPen = new Pen(Color.Black);
            blackPen.Width = 2;
            redPen = new Pen(Color.Red);
            redPen.Width = 2;
            darkGoldPen = new Pen(Color.Black);
            darkGoldPen.Width = 2;
            fo = new Font("Arial", 15);
            br = Brushes.Black;
        }

        public Bitmap GetBitmap()
        {
            return bitmap;
        }

        public void clearSheet()
        {
            gr.Clear(Color.White);
        }

        public void drawVertex(Vertex v, int M, int LeftC, int UpC)
        {
            gr.FillEllipse(v.color, ((v.x - LeftC) / M - R), ((v.y - UpC) / M - R), 2 * R, 2 * R);
            gr.DrawEllipse(blackPen, ((v.x - LeftC) / M - R), ((v.y - UpC) / M - R), 2 * R, 2 * R);
            point = new PointF((v.x - LeftC) / M - 7 - v.num.Count() * 2, (v.y - UpC) / M - 11 + v.num.Count() * 2);
            Font fon = new Font("Arial", 16 - v.num.Count() * 2);
            gr.DrawString(v.num, fon, br, point);
        }

        public void drawSelectedVertex(int x, int y)
        {
            gr.DrawEllipse(redPen, (x - R), (y - R), 2 * R, 2 * R);
        }

        public void drawSelectedEdge(Vertex V1, Vertex V2, Edge E, int M, int LeftC, int UpC)
        {

            if (E.v1 == E.v2)
            {
                gr.DrawArc(redPen, ((V1.x - LeftC) / M - 2 * R), ((V1.y - UpC) / M - 2 * R), 2 * R, 2 * R, 90, 270);
                point = new PointF((V1.x - LeftC) / M - (int)(2.75 * R), (V1.y - UpC) / M - (int)(2.75 * R));
                drawVertex(V1, M, LeftC, UpC);
            }
            else
            {
                double angle = Math.Atan2((V1.x - LeftC) / M - (V2.x - LeftC) / M, (V1.y - UpC) / M - (V2.y - UpC) / M);
                gr.DrawLine(redPen, (V1.x - LeftC) / M, (V1.y - UpC) / M, (V2.x - LeftC) / M, (V2.y - UpC) / M);
                int xf, yf, Y, X, S, x, y;
                Y = Math.Abs((V1.y - UpC) / M - (V2.y - UpC) / M);          //Y-расстояние между точками А и В
                X = Math.Abs((V1.x - LeftC) / M - (V2.x - LeftC) / M);          //Х-расстояние между точками А и В
                S = (int)Math.Sqrt(Y * Y + X * X);  //расстояние между точками А и В
                yf = (Y * (S - 20)) / S;            //Y-расстояние между точками С и В
                xf = (X * yf) / Y;                  //Х-расстояние между точками С и В
                if ((V1.x - LeftC) / M >= (V2.x - LeftC) / M)
                {
                    x = (V1.x - LeftC) / M - xf;
                }
                else
                {
                    x = (V1.x - LeftC) / M + xf;
                }
                if ((V1.y - UpC) / M >= (V2.y - UpC) / M)
                {
                    y = (V1.y - UpC) / M - yf;
                }
                else
                {
                    y = (V1.y - UpC) / M + yf;
                }
                gr.DrawLine(redPen, x, y, Convert.ToInt32(x + 15 * Math.Sin(0.3 + angle)), Convert.ToInt32(y + 15 * Math.Cos(0.3 + angle)));
                gr.DrawLine(redPen, x, y, Convert.ToInt32(x + 15 * Math.Sin(angle - 0.3)), Convert.ToInt32(y + 15 * Math.Cos(angle - 0.3)));
                if ((V1.x - LeftC) / M >= (V2.x - LeftC) / M)
                {
                    x = (V2.x - LeftC) / M + xf;
                }
                else
                {
                    x = (V2.x - LeftC) / M - xf;
                }
                if ((V1.y - UpC) / M >= (V2.y - UpC) / M)
                {
                    y = (V2.y - UpC) / M + yf;
                }
                else
                {
                    y = (V2.y - UpC) / M - yf;
                }
                if (E.Orient == false)
                {
                    gr.DrawLine(redPen, x, y, Convert.ToInt32(x - 15 * Math.Sin(0.3 + angle)), Convert.ToInt32(y - 15 * Math.Cos(0.3 + angle)));
                    gr.DrawLine(redPen, x, y, Convert.ToInt32(x - 15 * Math.Sin(angle - 0.3)), Convert.ToInt32(y - 15 * Math.Cos(angle - 0.3)));
                    point = new PointF(((V1.x - LeftC) / M + (V2.x - LeftC) / M) / 2, ((V1.y - UpC) / M + (V2.y - UpC) / M) / 2);
                }
                drawVertex(V1, M, LeftC, UpC);
                drawVertex(V2, M, LeftC, UpC);

            }
        }

        public void drawEdge(Vertex V1, Vertex V2, Edge E, int numberE, int M, int LeftC, int UpC)
        {
            if (E.v1 == E.v2)
            {
                gr.DrawArc(darkGoldPen, ((V1.x - LeftC) / M - 2 * R), ((V1.y - UpC) / M - 2 * R), 2 * R, 2 * R, 90, 270);
                point = new PointF((V1.x - LeftC) / M - (int)(2.75 * R), (V1.y - UpC) / M - (int)(2.75 * R));
                gr.DrawString(E.num, fo, br, point);
                drawVertex(V1, M, LeftC, UpC);
            }
            else
            {
                double angle = Math.Atan2((V1.x - LeftC) / M - (V2.x - LeftC) / M, (V1.y - UpC) / M - (V2.y - UpC) / M);
                gr.DrawLine(blackPen, (V1.x - LeftC) / M, (V1.y - UpC) / M, (V2.x - LeftC) / M, (V2.y - UpC) / M);
                int xf, yf, Y, X, S, x, y;
                Y = Math.Abs((V1.y - UpC) / M - (V2.y - UpC) / M);          //Y-расстояние между точками А и В
                X = Math.Abs((V1.x - LeftC) / M - (V2.x - LeftC) / M);          //Х-расстояние между точками А и В
                S = (int)Math.Sqrt(Y * Y + X * X);  //расстояние между точками А и В
                yf = (Y * (S - 20)) / S;            //Y-расстояние между точками С и В
                if (Y != 0)
                {
                    xf = (X * yf) / Y;
                }
                else
                {
                    xf = 20;
                }

                //Х-расстояние между точками С и В
                if ((V1.x - LeftC) / M >= (V2.x - LeftC) / M)
                {
                    x = (V1.x - LeftC) / M - xf;
                }
                else
                {
                    x = (V1.x - LeftC) / M + xf;
                }
                if ((V1.y - UpC) / M >= (V2.y - UpC) / M)
                {
                    y = (V1.y - UpC) / M - yf;
                }
                else
                {
                    y = (V1.y - UpC) / M + yf;
                }
                gr.DrawLine(blackPen, x, y, Convert.ToInt32(x + 15 * Math.Sin(0.3 + angle)), Convert.ToInt32(y + 15 * Math.Cos(0.3 + angle)));
                gr.DrawLine(blackPen, x, y, Convert.ToInt32(x + 15 * Math.Sin(angle - 0.3)), Convert.ToInt32(y + 15 * Math.Cos(angle - 0.3)));
                if ((V1.x - LeftC) / M >= (V2.x - LeftC) / M)
                {
                    x = (V2.x - LeftC) / M + xf;
                }
                else
                {
                    x = (V2.x - LeftC) / M - xf;
                }
                if ((V1.y - UpC) / M >= (V2.y - UpC) / M)
                {
                    y = (V2.y - UpC) / M + yf;
                }
                else
                {
                    y = (V2.y - UpC) / M - yf;
                }
                gr.DrawLine(blackPen, x, y, Convert.ToInt32(x - 15 * Math.Sin(0.3 + angle)), Convert.ToInt32(y - 15 * Math.Cos(0.3 + angle)));
                gr.DrawLine(blackPen, x, y, Convert.ToInt32(x - 15 * Math.Sin(angle - 0.3)), Convert.ToInt32(y - 15 * Math.Cos(angle - 0.3)));
                point = new PointF(((V1.x - LeftC) / M + (V2.x - LeftC) / M) / 2 - E.num.Count() * 2 + 2, ((V1.y - UpC) / M + (V2.y - UpC) / M) / 2);
                gr.DrawString(E.num, fo, br, point);
                if (E.w != 1)
                {
                    if ((Math.Abs(V1.y - V2.y)) > (Math.Abs(V1.x - V2.x)))
                    {
                        point = new PointF(((V1.x - LeftC) / M + (V2.x - LeftC) / M) / 2 - E.num.Count() * 2 - 15, ((V1.y - UpC) / M + (V2.y - UpC) / M) / 2);
                    }
                    else
                    {
                        point = new PointF(((V1.x - LeftC) / M + (V2.x - LeftC) / M) / 2 - E.num.Count() * 2, ((V1.y - UpC) / M + (V2.y - UpC) / M) / 2 - 18);
                    }
                    gr.DrawString("" + (char)(E.w + 48), fo, br, point);
                }
                drawVertex(V1, M, LeftC, UpC);
                drawVertex(V2, M, LeftC, UpC);
            }
        }

        public void drawOEdge(Vertex V1, Vertex V2, Edge E, int numberE, int M, int LeftC, int UpC)
        {
            if (E.v1 == E.v2)
            {
                gr.DrawArc(darkGoldPen, ((V1.x - LeftC) / M - 2 * R), ((V1.y - UpC) / M - 2 * R), 2 * R, 2 * R, 90, 270);
                point = new PointF((V1.x - LeftC) / M - (int)(2.75 * R), (V1.y - UpC) / M - (int)(2.75 * R));
                gr.DrawString(E.num, fo, br, point);
                drawVertex(V1, M, LeftC, UpC);
            }
            else
            {
                double angle = Math.Atan2((V1.x - LeftC) / M - (V2.x - LeftC) / M, (V1.y - UpC) / M - (V2.y - UpC) / M);
                gr.DrawLine(blackPen, (V1.x - LeftC) / M, (V1.y - UpC) / M, (V2.x - LeftC) / M, (V2.y - UpC) / M);
                int xf, yf, Y, X, S, x, y;
                Y = Math.Abs((V1.y - UpC) / M - (V2.y - UpC) / M);
                X = Math.Abs((V1.x - LeftC) / M - (V2.x - LeftC) / M);
                S = (int)Math.Sqrt(Y * Y + X * X);
                yf = (Y * (S - 20)) / S;
                xf = (X * yf) / Y;
                if ((V1.x - LeftC) / M >= (V2.x - LeftC) / M)
                {
                    x = (V1.x - LeftC) / M - xf;
                }
                else
                {
                    x = (V1.x - LeftC) / M + xf;
                }
                if ((V1.y - UpC) / M >= (V2.y - UpC) / M)
                {
                    y = (V1.y - UpC) / M - yf;
                }
                else
                {
                    y = (V1.y - UpC) / M + yf;
                }
                gr.DrawLine(blackPen, x, y, Convert.ToInt32(x + 15 * Math.Sin(0.3 + angle)), Convert.ToInt32(y + 15 * Math.Cos(0.3 + angle)));
                gr.DrawLine(blackPen, x, y, Convert.ToInt32(x + 15 * Math.Sin(angle - 0.3)), Convert.ToInt32(y + 15 * Math.Cos(angle - 0.3)));
                point = new PointF(((V1.x - LeftC) / M + (V2.x - LeftC) / M) / 2 - E.num.Count() * 2 + 2, ((V1.y - UpC) / M + (V2.y - UpC) / M) / 2);
                gr.DrawString(E.num, fo, br, point);
                if (E.w != 1000)
                {
                    if ((Math.Abs(V1.y - V2.y)) > (Math.Abs(V1.x - V2.x)))
                    {
                        point = new PointF(((V1.x - LeftC) / M + (V2.x - LeftC) / M) / 2 - E.num.Count() * 2 - 15, ((V1.y - UpC) / M + (V2.y - UpC) / M) / 2);
                    }
                    else
                    {
                        point = new PointF(((V1.x - LeftC) / M + (V2.x - LeftC) / M) / 2 - E.num.Count() * 2, ((V1.y - UpC) / M + (V2.y - UpC) / M) / 2 - 18);
                    }
                    gr.DrawString("" + (char)(E.w + 48), fo, br, point);
                }
                drawVertex(V1, M, LeftC, UpC);
                drawVertex(V2, M, LeftC, UpC);
            }
        }

        public void drawALLGraph(List<Vertex> V, List<Edge> E, int M, int LeftC, int UpC)
        {
            //1056 *628
            for (int i = 0; i < M + 1; i++)
            {
                for (int j = 0; j < M + 1; j++)
                {
                    gr.DrawLine(Pens.Red, 0, (627 * j) / M, 1055, (627 * j) / M);
                    gr.DrawLine(Pens.Red, (1055 * i) / M, 0, (1055 * i) / M, 627);
                }
            }
            //рисуем ребра
            for (int i = 0; i < E.Count; i++)
            {
                if (E[i].v1 == E[i].v2)
                {
                    gr.DrawArc(darkGoldPen, ((V[E[i].v1].x - LeftC) / M - 2 * R), ((V[E[i].v1].y - UpC) / M - 2 * R), 2 * R, 2 * R, 90, 270);
                    point = new PointF((V[E[i].v1].x - LeftC) / M - (int)(2.75 * R), (V[E[i].v1].y - UpC) / M - (int)(2.75 * R));
                    gr.DrawString(E[i].num, fo, br, point);
                }
                else
                {
                    if (E[i].Orient == true)
                    {
                        drawOEdge(V[E[i].v1], V[E[i].v2], E[i], i, M, LeftC, UpC);
                    }
                    else
                    {
                        drawEdge(V[E[i].v1], V[E[i].v2], E[i], i, M, LeftC, UpC);
                    }
                    point = new PointF(((V[E[i].v1].x - LeftC) / M + (V[E[i].v2].x - LeftC) / M) / 2 - E[i].num.Count() * 2 + 2, ((V[E[i].v1].y - UpC) / M + (V[E[i].v2].y - UpC) / M) / 2);
                    gr.DrawString(E[i].num, fo, br, point);
                    /*                    gr.DrawLine(darkGoldPen, V[E[i].v1].x, V[E[i].v1].y, V[E[i].v2].x, V[E[i].v2].y);
                                        point = new PointF((V[E[i].v1].x + V[E[i].v2].x) / 2, (V[E[i].v1].y + V[E[i].v2].y) / 2);
                                        gr.DrawString(((char)('a' + i)).ToString(), fo, br, point);*/
                }
            }
            //рисуем вершины
            for (int i = 0; i < V.Count; i++)
            {
                drawVertex(V[i], M, LeftC, UpC);
            }
        }


        //заполняет матрицу смежности
        public void fillAdjacencyMatrix(int numberV, List<Edge> E, int[,] matrix)
        {
            for (int i = 0; i < numberV; i++)
                for (int j = 0; j < numberV; j++)
                    matrix[i, j] = 0;
            for (int i = 0; i < E.Count; i++)
            {
                matrix[E[i].v1, E[i].v2] = 1;
                matrix[E[i].v2, E[i].v1] = 1;
            }
        }

        //заполняет матрицу инцидентности
        public void fillIncidenceMatrix(int numberV, List<Edge> E, int[,] matrix)
        {
            for (int i = 0; i < numberV; i++)
                for (int j = 0; j < E.Count; j++)
                    matrix[i, j] = 0;
            for (int i = 0; i < E.Count; i++)
            {
                matrix[E[i].v1, i] = 1;
                matrix[E[i].v2, i] = 1;
            }
        }


    }
}