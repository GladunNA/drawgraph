﻿namespace CourseWork3_Graph
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.sheet = new System.Windows.Forms.PictureBox();
            this.InfoPanel = new System.Windows.Forms.Panel();
            this.VertexList = new System.Windows.Forms.ListBox();
            this.ChangeName = new System.Windows.Forms.Button();
            this.NewName = new System.Windows.Forms.TextBox();
            this.ColorBox = new System.Windows.Forms.GroupBox();
            this.ColorBox20 = new System.Windows.Forms.PictureBox();
            this.ColorBox19 = new System.Windows.Forms.PictureBox();
            this.ColorBox18 = new System.Windows.Forms.PictureBox();
            this.ColorBox17 = new System.Windows.Forms.PictureBox();
            this.ColorBox16 = new System.Windows.Forms.PictureBox();
            this.ColorBox15 = new System.Windows.Forms.PictureBox();
            this.ColorBox14 = new System.Windows.Forms.PictureBox();
            this.ColorBox13 = new System.Windows.Forms.PictureBox();
            this.ColorBox12 = new System.Windows.Forms.PictureBox();
            this.ColorBox11 = new System.Windows.Forms.PictureBox();
            this.ColorBox10 = new System.Windows.Forms.PictureBox();
            this.ColorBox9 = new System.Windows.Forms.PictureBox();
            this.ColorBox8 = new System.Windows.Forms.PictureBox();
            this.ColorBox7 = new System.Windows.Forms.PictureBox();
            this.ColorBox6 = new System.Windows.Forms.PictureBox();
            this.ColorBox5 = new System.Windows.Forms.PictureBox();
            this.ColorBox4 = new System.Windows.Forms.PictureBox();
            this.ColorBox3 = new System.Windows.Forms.PictureBox();
            this.ColorBox2 = new System.Windows.Forms.PictureBox();
            this.ColorBox1 = new System.Windows.Forms.PictureBox();
            this.Имя = new System.Windows.Forms.Label();
            this.Selected_Name = new System.Windows.Forms.Label();
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.SaveGraph = new System.Windows.Forms.Button();
            this.LoadGraph = new System.Windows.Forms.Button();
            this.MoveVertex = new System.Windows.Forms.Button();
            this.ShowMatrix = new System.Windows.Forms.Button();
            this.Loupe = new System.Windows.Forms.Button();
            this.DeleteVertex = new System.Windows.Forms.Button();
            this.NewOEdgeButton = new System.Windows.Forms.Button();
            this.NewEdgeButton = new System.Windows.Forms.Button();
            this.Choose = new System.Windows.Forms.Button();
            this.NewVertexButton = new System.Windows.Forms.Button();
            this.Matrix = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.closeMatrix = new System.Windows.Forms.Button();
            this.MinusVertex = new System.Windows.Forms.Button();
            this.PlusVertex = new System.Windows.Forms.Button();
            this.AdMat = new System.Windows.Forms.PictureBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.closeMatrix2 = new System.Windows.Forms.Button();
            this.InMat = new System.Windows.Forms.PictureBox();
            this.Mashtab = new System.Windows.Forms.Label();
            this.Maximaze = new System.Windows.Forms.Button();
            this.Minimaze = new System.Windows.Forms.Button();
            this.AlgorythmsPanel = new System.Windows.Forms.Panel();
            this.Clique = new System.Windows.Forms.Button();
            this.Painting = new System.Windows.Forms.Button();
            this.ChoosePath = new System.Windows.Forms.FolderBrowserDialog();
            this.SaveFile = new System.Windows.Forms.SaveFileDialog();
            this.OpenFile = new System.Windows.Forms.OpenFileDialog();
            this.PrintButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.sheet)).BeginInit();
            this.InfoPanel.SuspendLayout();
            this.ColorBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox1)).BeginInit();
            this.ControlPanel.SuspendLayout();
            this.Matrix.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AdMat)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.InMat)).BeginInit();
            this.AlgorythmsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // sheet
            // 
            this.sheet.Location = new System.Drawing.Point(483, 346);
            this.sheet.Name = "sheet";
            this.sheet.Size = new System.Drawing.Size(498, 177);
            this.sheet.TabIndex = 0;
            this.sheet.TabStop = false;
            this.sheet.MouseClick += new System.Windows.Forms.MouseEventHandler(this.sheet_MouseClick);
            // 
            // InfoPanel
            // 
            this.InfoPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.InfoPanel.Controls.Add(this.VertexList);
            this.InfoPanel.Controls.Add(this.ChangeName);
            this.InfoPanel.Controls.Add(this.NewName);
            this.InfoPanel.Controls.Add(this.ColorBox);
            this.InfoPanel.Controls.Add(this.Имя);
            this.InfoPanel.Controls.Add(this.Selected_Name);
            this.InfoPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.InfoPanel.Location = new System.Drawing.Point(1095, 28);
            this.InfoPanel.Name = "InfoPanel";
            this.InfoPanel.Size = new System.Drawing.Size(227, 364);
            this.InfoPanel.TabIndex = 1;
            // 
            // VertexList
            // 
            this.VertexList.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.VertexList.FormattingEnabled = true;
            this.VertexList.ItemHeight = 16;
            this.VertexList.Location = new System.Drawing.Point(19, 82);
            this.VertexList.Name = "VertexList";
            this.VertexList.Size = new System.Drawing.Size(192, 100);
            this.VertexList.TabIndex = 5;
            // 
            // ChangeName
            // 
            this.ChangeName.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ChangeName.Location = new System.Drawing.Point(153, 44);
            this.ChangeName.Name = "ChangeName";
            this.ChangeName.Size = new System.Drawing.Size(75, 23);
            this.ChangeName.TabIndex = 4;
            this.ChangeName.Text = "button1";
            this.ChangeName.UseVisualStyleBackColor = true;
            this.ChangeName.Click += new System.EventHandler(this.ChangeName_Click);
            // 
            // NewName
            // 
            this.NewName.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NewName.Location = new System.Drawing.Point(72, 45);
            this.NewName.Name = "NewName";
            this.NewName.Size = new System.Drawing.Size(75, 22);
            this.NewName.TabIndex = 3;
            // 
            // ColorBox
            // 
            this.ColorBox.Controls.Add(this.ColorBox20);
            this.ColorBox.Controls.Add(this.ColorBox19);
            this.ColorBox.Controls.Add(this.ColorBox18);
            this.ColorBox.Controls.Add(this.ColorBox17);
            this.ColorBox.Controls.Add(this.ColorBox16);
            this.ColorBox.Controls.Add(this.ColorBox15);
            this.ColorBox.Controls.Add(this.ColorBox14);
            this.ColorBox.Controls.Add(this.ColorBox13);
            this.ColorBox.Controls.Add(this.ColorBox12);
            this.ColorBox.Controls.Add(this.ColorBox11);
            this.ColorBox.Controls.Add(this.ColorBox10);
            this.ColorBox.Controls.Add(this.ColorBox9);
            this.ColorBox.Controls.Add(this.ColorBox8);
            this.ColorBox.Controls.Add(this.ColorBox7);
            this.ColorBox.Controls.Add(this.ColorBox6);
            this.ColorBox.Controls.Add(this.ColorBox5);
            this.ColorBox.Controls.Add(this.ColorBox4);
            this.ColorBox.Controls.Add(this.ColorBox3);
            this.ColorBox.Controls.Add(this.ColorBox2);
            this.ColorBox.Controls.Add(this.ColorBox1);
            this.ColorBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ColorBox.Location = new System.Drawing.Point(12, 198);
            this.ColorBox.Name = "ColorBox";
            this.ColorBox.Size = new System.Drawing.Size(200, 160);
            this.ColorBox.TabIndex = 2;
            this.ColorBox.TabStop = false;
            this.ColorBox.Text = "Выберите цвет";
            // 
            // ColorBox20
            // 
            this.ColorBox20.Location = new System.Drawing.Point(107, 124);
            this.ColorBox20.Name = "ColorBox20";
            this.ColorBox20.Size = new System.Drawing.Size(17, 25);
            this.ColorBox20.TabIndex = 19;
            this.ColorBox20.TabStop = false;
            this.ColorBox20.Click += new System.EventHandler(this.ColorBox20_Click);
            // 
            // ColorBox19
            // 
            this.ColorBox19.Location = new System.Drawing.Point(84, 124);
            this.ColorBox19.Name = "ColorBox19";
            this.ColorBox19.Size = new System.Drawing.Size(17, 25);
            this.ColorBox19.TabIndex = 18;
            this.ColorBox19.TabStop = false;
            this.ColorBox19.Click += new System.EventHandler(this.ColorBox19_Click);
            // 
            // ColorBox18
            // 
            this.ColorBox18.Location = new System.Drawing.Point(61, 124);
            this.ColorBox18.Name = "ColorBox18";
            this.ColorBox18.Size = new System.Drawing.Size(17, 25);
            this.ColorBox18.TabIndex = 17;
            this.ColorBox18.TabStop = false;
            this.ColorBox18.Click += new System.EventHandler(this.ColorBox18_Click);
            // 
            // ColorBox17
            // 
            this.ColorBox17.Location = new System.Drawing.Point(35, 124);
            this.ColorBox17.Name = "ColorBox17";
            this.ColorBox17.Size = new System.Drawing.Size(17, 25);
            this.ColorBox17.TabIndex = 16;
            this.ColorBox17.TabStop = false;
            this.ColorBox17.Click += new System.EventHandler(this.ColorBox17_Click);
            // 
            // ColorBox16
            // 
            this.ColorBox16.Location = new System.Drawing.Point(12, 124);
            this.ColorBox16.Name = "ColorBox16";
            this.ColorBox16.Size = new System.Drawing.Size(17, 25);
            this.ColorBox16.TabIndex = 15;
            this.ColorBox16.TabStop = false;
            this.ColorBox16.Click += new System.EventHandler(this.ColorBox16_Click);
            // 
            // ColorBox15
            // 
            this.ColorBox15.Location = new System.Drawing.Point(107, 94);
            this.ColorBox15.Name = "ColorBox15";
            this.ColorBox15.Size = new System.Drawing.Size(17, 25);
            this.ColorBox15.TabIndex = 14;
            this.ColorBox15.TabStop = false;
            this.ColorBox15.Click += new System.EventHandler(this.ColorBox15_Click);
            // 
            // ColorBox14
            // 
            this.ColorBox14.Location = new System.Drawing.Point(84, 94);
            this.ColorBox14.Name = "ColorBox14";
            this.ColorBox14.Size = new System.Drawing.Size(17, 25);
            this.ColorBox14.TabIndex = 13;
            this.ColorBox14.TabStop = false;
            this.ColorBox14.Click += new System.EventHandler(this.ColorBox14_Click);
            // 
            // ColorBox13
            // 
            this.ColorBox13.Location = new System.Drawing.Point(61, 94);
            this.ColorBox13.Name = "ColorBox13";
            this.ColorBox13.Size = new System.Drawing.Size(17, 25);
            this.ColorBox13.TabIndex = 12;
            this.ColorBox13.TabStop = false;
            this.ColorBox13.Click += new System.EventHandler(this.ColorBox13_Click);
            // 
            // ColorBox12
            // 
            this.ColorBox12.Location = new System.Drawing.Point(35, 94);
            this.ColorBox12.Name = "ColorBox12";
            this.ColorBox12.Size = new System.Drawing.Size(17, 25);
            this.ColorBox12.TabIndex = 11;
            this.ColorBox12.TabStop = false;
            this.ColorBox12.Click += new System.EventHandler(this.ColorBox12_Click);
            // 
            // ColorBox11
            // 
            this.ColorBox11.Location = new System.Drawing.Point(12, 94);
            this.ColorBox11.Name = "ColorBox11";
            this.ColorBox11.Size = new System.Drawing.Size(17, 25);
            this.ColorBox11.TabIndex = 10;
            this.ColorBox11.TabStop = false;
            this.ColorBox11.Click += new System.EventHandler(this.ColorBox11_Click);
            // 
            // ColorBox10
            // 
            this.ColorBox10.Location = new System.Drawing.Point(107, 63);
            this.ColorBox10.Name = "ColorBox10";
            this.ColorBox10.Size = new System.Drawing.Size(17, 25);
            this.ColorBox10.TabIndex = 9;
            this.ColorBox10.TabStop = false;
            this.ColorBox10.Click += new System.EventHandler(this.ColorBox10_Click);
            // 
            // ColorBox9
            // 
            this.ColorBox9.Location = new System.Drawing.Point(84, 63);
            this.ColorBox9.Name = "ColorBox9";
            this.ColorBox9.Size = new System.Drawing.Size(17, 25);
            this.ColorBox9.TabIndex = 8;
            this.ColorBox9.TabStop = false;
            this.ColorBox9.Click += new System.EventHandler(this.ColorBox9_Click);
            // 
            // ColorBox8
            // 
            this.ColorBox8.Location = new System.Drawing.Point(61, 63);
            this.ColorBox8.Name = "ColorBox8";
            this.ColorBox8.Size = new System.Drawing.Size(17, 25);
            this.ColorBox8.TabIndex = 7;
            this.ColorBox8.TabStop = false;
            this.ColorBox8.Click += new System.EventHandler(this.ColorBox8_Click);
            // 
            // ColorBox7
            // 
            this.ColorBox7.Location = new System.Drawing.Point(35, 63);
            this.ColorBox7.Name = "ColorBox7";
            this.ColorBox7.Size = new System.Drawing.Size(17, 25);
            this.ColorBox7.TabIndex = 6;
            this.ColorBox7.TabStop = false;
            this.ColorBox7.Click += new System.EventHandler(this.ColorBox7_Click);
            // 
            // ColorBox6
            // 
            this.ColorBox6.Location = new System.Drawing.Point(12, 63);
            this.ColorBox6.Name = "ColorBox6";
            this.ColorBox6.Size = new System.Drawing.Size(17, 25);
            this.ColorBox6.TabIndex = 5;
            this.ColorBox6.TabStop = false;
            this.ColorBox6.Click += new System.EventHandler(this.ColorBox6_Click);
            // 
            // ColorBox5
            // 
            this.ColorBox5.Location = new System.Drawing.Point(107, 30);
            this.ColorBox5.Name = "ColorBox5";
            this.ColorBox5.Size = new System.Drawing.Size(17, 25);
            this.ColorBox5.TabIndex = 4;
            this.ColorBox5.TabStop = false;
            this.ColorBox5.Click += new System.EventHandler(this.ColorBox5_Click);
            // 
            // ColorBox4
            // 
            this.ColorBox4.Location = new System.Drawing.Point(84, 30);
            this.ColorBox4.Name = "ColorBox4";
            this.ColorBox4.Size = new System.Drawing.Size(17, 25);
            this.ColorBox4.TabIndex = 3;
            this.ColorBox4.TabStop = false;
            this.ColorBox4.Click += new System.EventHandler(this.ColorBox4_Click);
            // 
            // ColorBox3
            // 
            this.ColorBox3.Location = new System.Drawing.Point(61, 30);
            this.ColorBox3.Name = "ColorBox3";
            this.ColorBox3.Size = new System.Drawing.Size(17, 25);
            this.ColorBox3.TabIndex = 2;
            this.ColorBox3.TabStop = false;
            this.ColorBox3.Click += new System.EventHandler(this.ColorBox3_Click);
            // 
            // ColorBox2
            // 
            this.ColorBox2.Location = new System.Drawing.Point(35, 30);
            this.ColorBox2.Name = "ColorBox2";
            this.ColorBox2.Size = new System.Drawing.Size(17, 25);
            this.ColorBox2.TabIndex = 1;
            this.ColorBox2.TabStop = false;
            this.ColorBox2.Click += new System.EventHandler(this.ColorBox2_Click);
            // 
            // ColorBox1
            // 
            this.ColorBox1.Location = new System.Drawing.Point(12, 30);
            this.ColorBox1.Name = "ColorBox1";
            this.ColorBox1.Size = new System.Drawing.Size(17, 25);
            this.ColorBox1.TabIndex = 0;
            this.ColorBox1.TabStop = false;
            this.ColorBox1.Click += new System.EventHandler(this.ColorBox1_Click);
            // 
            // Имя
            // 
            this.Имя.AutoSize = true;
            this.Имя.Location = new System.Drawing.Point(20, 45);
            this.Имя.Name = "Имя";
            this.Имя.Size = new System.Drawing.Size(59, 20);
            this.Имя.TabIndex = 1;
            this.Имя.Text = "label1";
            // 
            // Selected_Name
            // 
            this.Selected_Name.AutoSize = true;
            this.Selected_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Selected_Name.Location = new System.Drawing.Point(20, 14);
            this.Selected_Name.Name = "Selected_Name";
            this.Selected_Name.Size = new System.Drawing.Size(70, 25);
            this.Selected_Name.TabIndex = 0;
            this.Selected_Name.Text = "label1";
            // 
            // ControlPanel
            // 
            this.ControlPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ControlPanel.Controls.Add(this.SaveGraph);
            this.ControlPanel.Controls.Add(this.LoadGraph);
            this.ControlPanel.Controls.Add(this.MoveVertex);
            this.ControlPanel.Controls.Add(this.ShowMatrix);
            this.ControlPanel.Controls.Add(this.Loupe);
            this.ControlPanel.Controls.Add(this.DeleteVertex);
            this.ControlPanel.Controls.Add(this.NewOEdgeButton);
            this.ControlPanel.Controls.Add(this.NewEdgeButton);
            this.ControlPanel.Controls.Add(this.Choose);
            this.ControlPanel.Controls.Add(this.NewVertexButton);
            this.ControlPanel.Location = new System.Drawing.Point(62, 160);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(174, 329);
            this.ControlPanel.TabIndex = 2;
            // 
            // SaveGraph
            // 
            this.SaveGraph.Image = ((System.Drawing.Image)(resources.GetObject("SaveGraph.Image")));
            this.SaveGraph.Location = new System.Drawing.Point(12, 246);
            this.SaveGraph.Name = "SaveGraph";
            this.SaveGraph.Size = new System.Drawing.Size(75, 62);
            this.SaveGraph.TabIndex = 9;
            this.SaveGraph.Text = "button1";
            this.SaveGraph.UseVisualStyleBackColor = true;
            this.SaveGraph.Click += new System.EventHandler(this.SaveGraph_Click);
            // 
            // LoadGraph
            // 
            this.LoadGraph.Image = ((System.Drawing.Image)(resources.GetObject("LoadGraph.Image")));
            this.LoadGraph.Location = new System.Drawing.Point(89, 246);
            this.LoadGraph.Name = "LoadGraph";
            this.LoadGraph.Size = new System.Drawing.Size(75, 62);
            this.LoadGraph.TabIndex = 8;
            this.LoadGraph.Text = "button1";
            this.LoadGraph.UseVisualStyleBackColor = true;
            this.LoadGraph.Click += new System.EventHandler(this.LoadGraph_Click);
            // 
            // MoveVertex
            // 
            this.MoveVertex.Image = ((System.Drawing.Image)(resources.GetObject("MoveVertex.Image")));
            this.MoveVertex.Location = new System.Drawing.Point(12, 136);
            this.MoveVertex.Name = "MoveVertex";
            this.MoveVertex.Size = new System.Drawing.Size(61, 47);
            this.MoveVertex.TabIndex = 7;
            this.MoveVertex.Text = "button1";
            this.MoveVertex.UseVisualStyleBackColor = true;
            this.MoveVertex.Click += new System.EventHandler(this.MoveVertex_Click);
            // 
            // ShowMatrix
            // 
            this.ShowMatrix.Image = ((System.Drawing.Image)(resources.GetObject("ShowMatrix.Image")));
            this.ShowMatrix.Location = new System.Drawing.Point(75, 190);
            this.ShowMatrix.Name = "ShowMatrix";
            this.ShowMatrix.Size = new System.Drawing.Size(75, 52);
            this.ShowMatrix.TabIndex = 6;
            this.ShowMatrix.Text = "button1";
            this.ShowMatrix.UseVisualStyleBackColor = true;
            this.ShowMatrix.Click += new System.EventHandler(this.ShowMatrix_Click);
            // 
            // Loupe
            // 
            this.Loupe.Image = ((System.Drawing.Image)(resources.GetObject("Loupe.Image")));
            this.Loupe.Location = new System.Drawing.Point(113, 136);
            this.Loupe.Name = "Loupe";
            this.Loupe.Size = new System.Drawing.Size(51, 48);
            this.Loupe.TabIndex = 5;
            this.Loupe.Text = "button1";
            this.Loupe.UseVisualStyleBackColor = true;
            this.Loupe.Click += new System.EventHandler(this.Loupe_Click);
            // 
            // DeleteVertex
            // 
            this.DeleteVertex.Image = ((System.Drawing.Image)(resources.GetObject("DeleteVertex.Image")));
            this.DeleteVertex.Location = new System.Drawing.Point(12, 204);
            this.DeleteVertex.Name = "DeleteVertex";
            this.DeleteVertex.Size = new System.Drawing.Size(50, 47);
            this.DeleteVertex.TabIndex = 4;
            this.DeleteVertex.Text = "button1";
            this.DeleteVertex.UseVisualStyleBackColor = true;
            this.DeleteVertex.Click += new System.EventHandler(this.DeleteVertex_Click);
            // 
            // NewOEdgeButton
            // 
            this.NewOEdgeButton.Image = ((System.Drawing.Image)(resources.GetObject("NewOEdgeButton.Image")));
            this.NewOEdgeButton.Location = new System.Drawing.Point(56, 83);
            this.NewOEdgeButton.Name = "NewOEdgeButton";
            this.NewOEdgeButton.Size = new System.Drawing.Size(75, 47);
            this.NewOEdgeButton.TabIndex = 3;
            this.NewOEdgeButton.Text = "button1";
            this.NewOEdgeButton.UseVisualStyleBackColor = true;
            this.NewOEdgeButton.Click += new System.EventHandler(this.NewOEdgeButton_Click);
            // 
            // NewEdgeButton
            // 
            this.NewEdgeButton.Image = ((System.Drawing.Image)(resources.GetObject("NewEdgeButton.Image")));
            this.NewEdgeButton.Location = new System.Drawing.Point(12, 3);
            this.NewEdgeButton.Name = "NewEdgeButton";
            this.NewEdgeButton.Size = new System.Drawing.Size(75, 62);
            this.NewEdgeButton.TabIndex = 2;
            this.NewEdgeButton.Text = "button1";
            this.NewEdgeButton.UseVisualStyleBackColor = true;
            this.NewEdgeButton.Click += new System.EventHandler(this.NewEdgeButton_Click);
            // 
            // Choose
            // 
            this.Choose.Image = ((System.Drawing.Image)(resources.GetObject("Choose.Image")));
            this.Choose.Location = new System.Drawing.Point(94, 112);
            this.Choose.Name = "Choose";
            this.Choose.Size = new System.Drawing.Size(75, 42);
            this.Choose.TabIndex = 1;
            this.Choose.Text = "button1";
            this.Choose.UseVisualStyleBackColor = true;
            this.Choose.Click += new System.EventHandler(this.ChooseButton_Click);
            // 
            // NewVertexButton
            // 
            this.NewVertexButton.Image = ((System.Drawing.Image)(resources.GetObject("NewVertexButton.Image")));
            this.NewVertexButton.Location = new System.Drawing.Point(113, 25);
            this.NewVertexButton.Name = "NewVertexButton";
            this.NewVertexButton.Size = new System.Drawing.Size(37, 40);
            this.NewVertexButton.TabIndex = 0;
            this.NewVertexButton.Text = "button1";
            this.NewVertexButton.UseVisualStyleBackColor = true;
            this.NewVertexButton.Click += new System.EventHandler(this.NewVertexButton_Click);
            // 
            // Matrix
            // 
            this.Matrix.Controls.Add(this.tabPage1);
            this.Matrix.Controls.Add(this.tabPage2);
            this.Matrix.Location = new System.Drawing.Point(779, 57);
            this.Matrix.Name = "Matrix";
            this.Matrix.SelectedIndex = 0;
            this.Matrix.Size = new System.Drawing.Size(183, 105);
            this.Matrix.TabIndex = 5;
            this.Matrix.Visible = false;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.closeMatrix);
            this.tabPage1.Controls.Add(this.MinusVertex);
            this.tabPage1.Controls.Add(this.PlusVertex);
            this.tabPage1.Controls.Add(this.AdMat);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(175, 76);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // closeMatrix
            // 
            this.closeMatrix.Location = new System.Drawing.Point(149, 48);
            this.closeMatrix.Name = "closeMatrix";
            this.closeMatrix.Size = new System.Drawing.Size(16, 26);
            this.closeMatrix.TabIndex = 3;
            this.closeMatrix.Text = "button4";
            this.closeMatrix.UseVisualStyleBackColor = true;
            this.closeMatrix.Click += new System.EventHandler(this.closeMatrix_Click);
            // 
            // MinusVertex
            // 
            this.MinusVertex.Location = new System.Drawing.Point(30, 48);
            this.MinusVertex.Name = "MinusVertex";
            this.MinusVertex.Size = new System.Drawing.Size(12, 18);
            this.MinusVertex.TabIndex = 2;
            this.MinusVertex.Text = "button3";
            this.MinusVertex.UseVisualStyleBackColor = true;
            this.MinusVertex.Click += new System.EventHandler(this.MinusVertex_Click);
            // 
            // PlusVertex
            // 
            this.PlusVertex.Location = new System.Drawing.Point(9, 48);
            this.PlusVertex.Name = "PlusVertex";
            this.PlusVertex.Size = new System.Drawing.Size(13, 19);
            this.PlusVertex.TabIndex = 1;
            this.PlusVertex.Text = "button2";
            this.PlusVertex.UseVisualStyleBackColor = true;
            this.PlusVertex.Click += new System.EventHandler(this.PlusVertex_Click);
            // 
            // AdMat
            // 
            this.AdMat.Location = new System.Drawing.Point(6, 6);
            this.AdMat.Name = "AdMat";
            this.AdMat.Size = new System.Drawing.Size(78, 34);
            this.AdMat.TabIndex = 0;
            this.AdMat.TabStop = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.closeMatrix2);
            this.tabPage2.Controls.Add(this.InMat);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(175, 76);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // closeMatrix2
            // 
            this.closeMatrix2.Location = new System.Drawing.Point(153, 44);
            this.closeMatrix2.Name = "closeMatrix2";
            this.closeMatrix2.Size = new System.Drawing.Size(16, 26);
            this.closeMatrix2.TabIndex = 4;
            this.closeMatrix2.Text = "button4";
            this.closeMatrix2.UseVisualStyleBackColor = true;
            this.closeMatrix2.Click += new System.EventHandler(this.closeMatrix2_Click);
            // 
            // InMat
            // 
            this.InMat.Location = new System.Drawing.Point(14, 11);
            this.InMat.Name = "InMat";
            this.InMat.Size = new System.Drawing.Size(70, 27);
            this.InMat.TabIndex = 0;
            this.InMat.TabStop = false;
            // 
            // Mashtab
            // 
            this.Mashtab.AutoSize = true;
            this.Mashtab.Location = new System.Drawing.Point(905, 257);
            this.Mashtab.Name = "Mashtab";
            this.Mashtab.Size = new System.Drawing.Size(46, 17);
            this.Mashtab.TabIndex = 6;
            this.Mashtab.Text = "label1";
            // 
            // Maximaze
            // 
            this.Maximaze.Location = new System.Drawing.Point(341, 215);
            this.Maximaze.Name = "Maximaze";
            this.Maximaze.Size = new System.Drawing.Size(50, 42);
            this.Maximaze.TabIndex = 7;
            this.Maximaze.Text = "button1";
            this.Maximaze.UseVisualStyleBackColor = true;
            this.Maximaze.Click += new System.EventHandler(this.Maximaze_Click);
            // 
            // Minimaze
            // 
            this.Minimaze.Location = new System.Drawing.Point(416, 213);
            this.Minimaze.Name = "Minimaze";
            this.Minimaze.Size = new System.Drawing.Size(53, 43);
            this.Minimaze.TabIndex = 8;
            this.Minimaze.Text = "button2";
            this.Minimaze.UseVisualStyleBackColor = true;
            this.Minimaze.Click += new System.EventHandler(this.Minimaze_Click);
            // 
            // AlgorythmsPanel
            // 
            this.AlgorythmsPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AlgorythmsPanel.Controls.Add(this.PrintButton);
            this.AlgorythmsPanel.Controls.Add(this.Clique);
            this.AlgorythmsPanel.Controls.Add(this.Painting);
            this.AlgorythmsPanel.Location = new System.Drawing.Point(1098, 425);
            this.AlgorythmsPanel.Name = "AlgorythmsPanel";
            this.AlgorythmsPanel.Size = new System.Drawing.Size(225, 132);
            this.AlgorythmsPanel.TabIndex = 9;
            // 
            // Clique
            // 
            this.Clique.Image = ((System.Drawing.Image)(resources.GetObject("Clique.Image")));
            this.Clique.Location = new System.Drawing.Point(52, 3);
            this.Clique.Name = "Clique";
            this.Clique.Size = new System.Drawing.Size(37, 40);
            this.Clique.TabIndex = 2;
            this.Clique.Text = "button1";
            this.Clique.UseVisualStyleBackColor = true;
            this.Clique.Click += new System.EventHandler(this.Clique_Click);
            // 
            // Painting
            // 
            this.Painting.Image = ((System.Drawing.Image)(resources.GetObject("Painting.Image")));
            this.Painting.Location = new System.Drawing.Point(9, 3);
            this.Painting.Name = "Painting";
            this.Painting.Size = new System.Drawing.Size(37, 40);
            this.Painting.TabIndex = 1;
            this.Painting.Text = "button1";
            this.Painting.UseVisualStyleBackColor = true;
            this.Painting.Click += new System.EventHandler(this.Painting_Click);
            // 
            // ChoosePath
            // 
            this.ChoosePath.Description = "Выберите папку для сохранения графа";
            // 
            // OpenFile
            // 
            this.OpenFile.FileName = "openFileDialog1";
            // 
            // PrintButton
            // 
            this.PrintButton.Image = ((System.Drawing.Image)(resources.GetObject("PrintButton.Image")));
            this.PrintButton.Location = new System.Drawing.Point(171, 3);
            this.PrintButton.Name = "PrintButton";
            this.PrintButton.Size = new System.Drawing.Size(37, 40);
            this.PrintButton.TabIndex = 3;
            this.PrintButton.Text = "button1";
            this.PrintButton.UseVisualStyleBackColor = true;
            this.PrintButton.Click += new System.EventHandler(this.PrintButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1346, 520);
            this.Controls.Add(this.AlgorythmsPanel);
            this.Controls.Add(this.Minimaze);
            this.Controls.Add(this.Maximaze);
            this.Controls.Add(this.Mashtab);
            this.Controls.Add(this.Matrix);
            this.Controls.Add(this.ControlPanel);
            this.Controls.Add(this.InfoPanel);
            this.Controls.Add(this.sheet);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sheet)).EndInit();
            this.InfoPanel.ResumeLayout(false);
            this.InfoPanel.PerformLayout();
            this.ColorBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorBox1)).EndInit();
            this.ControlPanel.ResumeLayout(false);
            this.Matrix.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AdMat)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.InMat)).EndInit();
            this.AlgorythmsPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox sheet;
        private System.Windows.Forms.Panel InfoPanel;
        private System.Windows.Forms.TextBox NewName;
        private System.Windows.Forms.GroupBox ColorBox;
        private System.Windows.Forms.Label Имя;
        private System.Windows.Forms.Label Selected_Name;
        private System.Windows.Forms.Button ChangeName;
        private System.Windows.Forms.ListBox VertexList;
        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.Button NewVertexButton;
        private System.Windows.Forms.Button Choose;
        private System.Windows.Forms.Button NewOEdgeButton;
        private System.Windows.Forms.Button NewEdgeButton;
        private System.Windows.Forms.Button DeleteVertex;
        private System.Windows.Forms.Button Loupe;
        private System.Windows.Forms.Button ShowMatrix;
        private System.Windows.Forms.Button MoveVertex;
        private System.Windows.Forms.TabControl Matrix;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.PictureBox AdMat;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button closeMatrix;
        private System.Windows.Forms.Button MinusVertex;
        private System.Windows.Forms.Button PlusVertex;
        private System.Windows.Forms.PictureBox InMat;
        private System.Windows.Forms.Button closeMatrix2;
        private System.Windows.Forms.Label Mashtab;
        private System.Windows.Forms.Button Maximaze;
        private System.Windows.Forms.Button Minimaze;
        private System.Windows.Forms.PictureBox ColorBox20;
        private System.Windows.Forms.PictureBox ColorBox19;
        private System.Windows.Forms.PictureBox ColorBox18;
        private System.Windows.Forms.PictureBox ColorBox17;
        private System.Windows.Forms.PictureBox ColorBox16;
        private System.Windows.Forms.PictureBox ColorBox15;
        private System.Windows.Forms.PictureBox ColorBox14;
        private System.Windows.Forms.PictureBox ColorBox13;
        private System.Windows.Forms.PictureBox ColorBox12;
        private System.Windows.Forms.PictureBox ColorBox11;
        private System.Windows.Forms.PictureBox ColorBox10;
        private System.Windows.Forms.PictureBox ColorBox9;
        private System.Windows.Forms.PictureBox ColorBox8;
        private System.Windows.Forms.PictureBox ColorBox7;
        private System.Windows.Forms.PictureBox ColorBox6;
        private System.Windows.Forms.PictureBox ColorBox5;
        private System.Windows.Forms.PictureBox ColorBox4;
        private System.Windows.Forms.PictureBox ColorBox3;
        private System.Windows.Forms.PictureBox ColorBox2;
        private System.Windows.Forms.PictureBox ColorBox1;
        private System.Windows.Forms.Panel AlgorythmsPanel;
        private System.Windows.Forms.Button Painting;
        private System.Windows.Forms.Button Clique;
        private System.Windows.Forms.Button LoadGraph;
        private System.Windows.Forms.Button SaveGraph;
        private System.Windows.Forms.FolderBrowserDialog ChoosePath;
        private System.Windows.Forms.SaveFileDialog SaveFile;
        private System.Windows.Forms.OpenFileDialog OpenFile;
        private System.Windows.Forms.Button PrintButton;
    }
}

