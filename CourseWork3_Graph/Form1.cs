﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace CourseWork3_Graph
{
    public partial class Form1 : Form
    {
        DrawGraph G;
        List<Vertex> V;
        List<Edge> E;
        String currentFolder = "";

        int choosen1;
        int choosen2;
        int choosenEd = -1;
        int choosinginmatx = -1;
        int choosinginmaty = -1;

        int num = 0;
        int numE = 0;

        public List<List<int>> M = new List<List<int>>();
        public int LenM;

        public int Mash = 3;
        public int MMM = 4;
        public int LeftC = 0;
        public int UpC = 0;

        public List<int> Findways(List<int> Founded, int selected, List<int> Prev, int way, int start)
        {
            for (int i = 0; i < V[selected].contacts.Count(); i++)
            {
                if (E[V[selected].contacts[i]].v1 == selected)
                {
                    if ((E[V[selected].contacts[i]].w + way < Founded[E[V[selected].contacts[i]].v2]) || (Founded[E[V[selected].contacts[i]].v2] == -1))
                    {
                        Founded[E[V[selected].contacts[i]].v2] = E[V[selected].contacts[i]].w + way;
                        Prev[E[V[selected].contacts[i]].v2] = selected;
                        Prev = Findways(Founded, E[V[selected].contacts[i]].v2, Prev, Founded[E[V[selected].contacts[i]].v2], start);
                    }
                }
                else
                {
                    if ((E[V[selected].contacts[i]].w + way < Founded[E[V[selected].contacts[i]].v1]) || (Founded[E[V[selected].contacts[i]].v1] == -1))
                    {
                        Founded[E[V[selected].contacts[i]].v1] = E[V[selected].contacts[i]].w + way;
                        Prev[E[V[selected].contacts[i]].v1] = selected;
                        Prev = Findways(Founded, E[V[selected].contacts[i]].v1, Prev, Founded[E[V[selected].contacts[i]].v1], start);
                    }
                }
            }
            return Prev;          
        }

        public Form1()
        {
            InitializeComponent();

            ControlPanel.Top = 75;
            ControlPanel.Left = 25;
            ControlPanel.Height = 285;
            ControlPanel.Width = 115;
            sheet.Top = 25;
            sheet.Left = ControlPanel.Left + ControlPanel.Width + 25;
            sheet.Height = 628;
            sheet.Width = 1056;
            InfoPanel.Top = 25;
            InfoPanel.Left = 25 + sheet.Left + sheet.Width;
            InfoPanel.Width = 200;
            InfoPanel.Height = 325;
            AlgorythmsPanel.Top = InfoPanel.Top + InfoPanel.Height + 10;
            AlgorythmsPanel.Left = InfoPanel.Left;
            AlgorythmsPanel.Width = InfoPanel.Width;
            Selected_Name.Top = 5;
            Selected_Name.Left = 5;
            Selected_Name.Text = "Ничего не выбрано";
            Имя.Top = Selected_Name.Top + 30;
            Имя.Left = Selected_Name.Left;
            Имя.Text = "Имя: ";
            NewName.Top = Имя.Top;
            NewName.Left = Имя.Left + 50;
            NewName.Width = 70;
            ChangeName.Top = NewName.Top;
            ChangeName.Left = NewName.Left + NewName.Width + 5;
            ChangeName.Text = "Click!";

            Minimaze.Width = 50;
            Minimaze.Height = 50;
            Minimaze.Left = sheet.Left + sheet.Width - 10- Minimaze.Width;
            Minimaze.Top = sheet.Top + sheet.Height - 10 - Minimaze.Height;
            Minimaze.Text = "-";
            Maximaze.Width = 50;
            Maximaze.Height = 50;
            Maximaze.Left = sheet.Left + sheet.Width - 10 - Maximaze.Width - Minimaze.Width - 10;
            Maximaze.Top = sheet.Top + sheet.Height - 10 - Maximaze.Height;
            Maximaze.Text = "+";
            Mashtab.Top = sheet.Top + sheet.Height + 5;
            Mashtab.Left = sheet.Left + sheet.Width - 100;
            Mashtab.Text = "Масштаб 100%";

            Choose.Left = 5;
            Choose.Top = 5;
            Choose.Height = 50;
            Choose.Width = 50;
            Choose.Text = "";
            NewVertexButton.Left = 60;
            NewVertexButton.Top = 5;
            NewVertexButton.Height = 50;
            NewVertexButton.Width = 50;
            NewVertexButton.Text = "";
            NewOEdgeButton.Left = 5;
            NewOEdgeButton.Top = 60;
            NewOEdgeButton.Height = 50;
            NewOEdgeButton.Width = 50;
            NewOEdgeButton.Text = "";
            NewEdgeButton.Left = 60;
            NewEdgeButton.Top = 60;
            NewEdgeButton.Height = 50;
            NewEdgeButton.Width = 50;
            NewEdgeButton.Text = "";
            MoveVertex.Left = 5;
            MoveVertex.Top = 115;
            MoveVertex.Height = 50;
            MoveVertex.Width = 50;
            MoveVertex.Text = "";
            DeleteVertex.Left = 60;
            DeleteVertex.Top = 115;
            DeleteVertex.Height = 50;
            DeleteVertex.Width = 50;
            DeleteVertex.Text = "";
            ShowMatrix.Left = 5;
            ShowMatrix.Top = 170;
            ShowMatrix.Height = 50;
            ShowMatrix.Width = 50;
            ShowMatrix.Text = "";
            Loupe.Left = 60;
            Loupe.Top = 170;
            Loupe.Height = 50;
            Loupe.Width = 50;
            Loupe.Text = "";
            SaveGraph.Left = 5;
            SaveGraph.Top = 225;
            SaveGraph.Height = 50;
            SaveGraph.Width = 50;
            SaveGraph.Text = "";
            LoadGraph.Left = 60;
            LoadGraph.Top = 225;
            LoadGraph.Height = 50;
            LoadGraph.Width = 50;
            LoadGraph.Text = "";

            Painting.Left = 5;
            Painting.Top = 5;
            Painting.Height = 50;
            Painting.Width = 50;
            Painting.Text = "";
            Clique.Left = 60;
            Clique.Top = 5;
            Clique.Height = 50;
            Clique.Width = 50;
            Clique.Text = "";
            PrintButton.Left = AlgorythmsPanel.Width - 5 - 50;
            PrintButton.Top = Painting.Top;
            PrintButton.Width = 50;
            PrintButton.Height = 50;
            PrintButton.Text = "";

            ColorBox1.Left = 10;
            ColorBox1.Top = 17;
            ColorBox1.Height = 20;
            ColorBox1.Width = 20;
            ColorBox1.BackColor = Color.White;
            ColorBox2.Left = 37;
            ColorBox2.Top = 17;
            ColorBox2.Height = 20;
            ColorBox2.Width = 20;
            ColorBox2.BackColor = Color.LightGray;
            ColorBox3.Left = 64;
            ColorBox3.Top = 17;
            ColorBox3.Height = 20;
            ColorBox3.Width = 20;
            ColorBox3.BackColor = Color.DarkGray;
            ColorBox4.Left = 91;
            ColorBox4.Top = 17;
            ColorBox4.Height = 20;
            ColorBox4.Width = 20;
            ColorBox4.BackColor = Color.Gray;
            ColorBox5.Left = 118;
            ColorBox5.Top = 17;
            ColorBox5.Height = 20;
            ColorBox5.Width = 20;
            ColorBox5.BackColor = Color.Black;
            ColorBox6.Left = 10;
            ColorBox6.Top = 44;
            ColorBox6.Height = 20;
            ColorBox6.Width = 20;
            ColorBox6.BackColor = Color.Red;
            ColorBox7.Left = 37;
            ColorBox7.Top = 44;
            ColorBox7.Height = 20;
            ColorBox7.Width = 20;
            ColorBox7.BackColor = Color.Orange;
            ColorBox8.Left = 64;
            ColorBox8.Top = 44;
            ColorBox8.Height = 20;
            ColorBox8.Width = 20;
            ColorBox8.BackColor = Color.Yellow;
            ColorBox9.Left = 91;
            ColorBox9.Top = 44;
            ColorBox9.Height = 20;
            ColorBox9.Width = 20;
            ColorBox9.BackColor = Color.Lime;
            ColorBox10.Left = 118;
            ColorBox10.Top = 44;
            ColorBox10.Height = 20;
            ColorBox10.Width = 20;
            ColorBox10.BackColor = Color.LimeGreen;
            ColorBox11.Left = 10;
            ColorBox11.Top = 71;
            ColorBox11.Height = 20;
            ColorBox11.Width = 20;
            ColorBox11.BackColor = Color.Indigo;
            ColorBox12.Left = 37;
            ColorBox12.Top = 71;
            ColorBox12.Height = 20;
            ColorBox12.Width = 20;
            ColorBox12.BackColor = Color.Blue;
            ColorBox13.Left = 64;
            ColorBox13.Top = 71;
            ColorBox13.Height = 20;
            ColorBox13.Width = 20;
            ColorBox13.BackColor = Color.LightBlue;
            ColorBox14.Left = 91;
            ColorBox14.Top = 71;
            ColorBox14.Height = 20;
            ColorBox14.Width = 20;
            ColorBox14.BackColor = Color.Aquamarine;
            ColorBox15.Left = 118;
            ColorBox15.Top = 71;
            ColorBox15.Height = 20;
            ColorBox15.Width = 20;
            ColorBox15.BackColor = Color.Green;
            ColorBox16.Left = 10;
            ColorBox16.Top = 98;
            ColorBox16.Height = 20;
            ColorBox16.Width = 20;
            ColorBox16.BackColor = Color.Purple;
            ColorBox17.Left = 37;
            ColorBox17.Top = 98;
            ColorBox17.Height = 20;
            ColorBox17.Width = 20;
            ColorBox17.BackColor = Color.Violet;
            ColorBox18.Left = 64;
            ColorBox18.Top = 98;
            ColorBox18.Height = 20;
            ColorBox18.Width = 20;
            ColorBox18.BackColor = Color.Pink;
            ColorBox19.Left = 91;
            ColorBox19.Top = 98;
            ColorBox19.Height = 20;
            ColorBox19.Width = 20;
            ColorBox19.BackColor = Color.DarkRed;
            ColorBox20.Left = 118;
            ColorBox20.Top = 98;
            ColorBox20.Height = 20;
            ColorBox20.Width = 20;
            ColorBox20.BackColor = Color.Brown;
            PlusVertex.Height = 20;
            PlusVertex.Width = 20;
            PlusVertex.Text = "+";
            MinusVertex.Height = 20;
            MinusVertex.Width = 20;
            MinusVertex.Text = "-";
            closeMatrix.Height = 20;
            closeMatrix.Width = 20;
            closeMatrix.Text = "x";
            closeMatrix2.Height = 20;
            closeMatrix2.Width = 20;
            closeMatrix2.Text = "x";

            V = new List<Vertex>();
            G = new DrawGraph(sheet.Width, sheet.Height);
            E = new List<Edge>();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Имя.Visible = false;
            ChangeName.Visible = false;
            NewName.Visible = false;
            VertexList.Visible = false;
            //TestMatrix.Visible = false;
            M.Add(new List<int>());
            M[0].Add(0);
            ColorBox.Visible = false;
            
        }

        private void NewVertexButton_Click(object sender, EventArgs e)
        {
            Имя.Visible = false;
            ChangeName.Visible = false;
            NewName.Visible = false;
            VertexList.Visible = false;
            ColorBox.Visible = false;
            Selected_Name.Text = "Ничего не выбрано";
            NewVertexButton.Enabled = false;
            Choose.Enabled = true;
            NewEdgeButton.Enabled = true;
            NewOEdgeButton.Enabled = true;
            DeleteVertex.Enabled = true;
            Loupe.Enabled = true;
            ShowMatrix.Enabled = true;
            MoveVertex.Enabled = true;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        private void ChooseButton_Click(object sender, EventArgs e)
        {
            Имя.Visible = false;
            ChangeName.Visible = false;
            NewName.Visible = false;
            VertexList.Visible = false;
            Selected_Name.Text = "Ничего не выбрано";
            NewVertexButton.Enabled = true;
            Choose.Enabled = false;
            ColorBox.Visible = false;
            NewEdgeButton.Enabled = true;
            NewOEdgeButton.Enabled = true;
            DeleteVertex.Enabled = true;
            Loupe.Enabled = true;
            ShowMatrix.Enabled = true;
            MoveVertex.Enabled = true;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
            choosen1 = -1;
        }

        private void NewOEdgeButton_Click(object sender, EventArgs e)
        {
            Имя.Visible = false;
            ChangeName.Visible = false;
            NewName.Visible = false;
            VertexList.Visible = false;
            ColorBox.Visible = false;
            Selected_Name.Text = "Ничего не выбрано";
            NewVertexButton.Enabled = true;
            Choose.Enabled = true;
            NewEdgeButton.Enabled = true;
            NewOEdgeButton.Enabled = false;
            DeleteVertex.Enabled = true;
            Loupe.Enabled = true;
            ShowMatrix.Enabled = true;
            MoveVertex.Enabled = true;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
            choosen1 = -1;
            choosen2 = -1;
        }

        private void NewEdgeButton_Click(object sender, EventArgs e)
        {
            Имя.Visible = false;
            ChangeName.Visible = false;
            NewName.Visible = false;
            VertexList.Visible = false;
            ColorBox.Visible = false;
            Selected_Name.Text = "Ничего не выбрано";
            NewVertexButton.Enabled = true;
            Choose.Enabled = true;
            NewEdgeButton.Enabled = false;
            NewOEdgeButton.Enabled = true;
            DeleteVertex.Enabled = true;
            Loupe.Enabled = true;
            ShowMatrix.Enabled = true;
            MoveVertex.Enabled = true;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
            choosen1 = -1;
            choosen2 = -1;

        }

        private void DeleteVertex_Click(object sender, EventArgs e)
        {
            Имя.Visible = false;
            ChangeName.Visible = false;
            NewName.Visible = false;
            VertexList.Visible = false;
            ColorBox.Visible = false;
            Selected_Name.Text = "Ничего не выбрано";
            NewVertexButton.Enabled = true;
            Choose.Enabled = true;
            NewEdgeButton.Enabled = true;
            NewOEdgeButton.Enabled = true;
            DeleteVertex.Enabled = false;
            Loupe.Enabled = true;
            ShowMatrix.Enabled = true;
            MoveVertex.Enabled = true;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        private void Loupe_Click(object sender, EventArgs e)
        {
            Имя.Visible = false;
            ChangeName.Visible = false;
            NewName.Visible = false;
            VertexList.Visible = false;
            ColorBox.Visible = false;
            Selected_Name.Text = "Ничего не выбрано";
            NewVertexButton.Enabled = true;
            Choose.Enabled = true;
            NewEdgeButton.Enabled = true;
            NewOEdgeButton.Enabled = true;
            DeleteVertex.Enabled = true;
            Loupe.Enabled = false;
            ShowMatrix.Enabled = true;
            MoveVertex.Enabled = true;
        }

        private void ShowMatrix_Click(object sender, EventArgs e)
        {
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            NewVertexButton.Enabled = true;
            Choose.Enabled = true;
            NewEdgeButton.Enabled = true;
            NewOEdgeButton.Enabled = true;
            DeleteVertex.Enabled = true;
            Loupe.Enabled = true;
            ShowMatrix.Enabled = false;
            MoveVertex.Enabled = true;
            Matrix.Visible = true;
            AdMat.Image = new Bitmap(2000, 2000);
            Graphics gra = Graphics.FromImage(AdMat.Image);
            gra.DrawString("", new Font("Microsoft Sans Serif", 8), Brushes.Black, 1, 1);
            gra.DrawLine(new Pen(Color.Black), 0, 19, 19, 19);
            gra.DrawLine(new Pen(Color.Black), 19, 0, 19, 19);
            gra.DrawLine(new Pen(Color.Black), 0, 0, 0, 19);
            gra.DrawLine(new Pen(Color.Black), 0, 0, 19, 0);
            Matrix.Height = 94;
            Matrix.Width = 250;
            AdMat.Height = 20;
            AdMat.Width = 20;
            for (int i = 1; i < M.Count(); i++)
            {
                gra.DrawLine(new Pen(Color.Black), 19 * i + 0, 19, 19 * i + 19, 19);
                gra.DrawLine(new Pen(Color.Black), 19 * i + 19, 0, 19 * i + 19, 19);
                gra.DrawLine(new Pen(Color.Black), 19 * i + 0, 0, 19 * i + 19, 0);
                gra.DrawString(V[M[0][i] - 1].num, new Font("Microsoft Sans Serif", 8), Brushes.Black, 1 + 19 * i, 1);
                AdMat.Width += 19;
                if (i > 12)
                {
                    Matrix.Width += 19;
                }
                gra.DrawLine(new Pen(Color.Black), 0, 19 * i + 19, 19, 19 * i + 19);
                gra.DrawLine(new Pen(Color.Black), 19, 19 * i + 0, 19, 19 * i + 19);
                gra.DrawLine(new Pen(Color.Black), 0, 19 * i + 0, 0, 19 * i + 19);
                gra.DrawString(V[M[0][i] - 1].num, new Font("Microsoft Sans Serif", 8), Brushes.Black, 1, 1 + 19 * i);
                AdMat.Height += 19;
                Matrix.Height += 19;
            }
            for (int i = 1; i < M.Count(); i++)
            {
                for (int j = 1; j < M[i].Count(); j++)
                {
                    string s = " ";
                    gra.DrawLine(new Pen(Color.Black), 19 * i + 0, 19 * j + 19, 19 * i + 19, 19 * j + 19);
                    gra.DrawLine(new Pen(Color.Black), 19 * i + 19, 19 * j + 0, 19 * i + 19, 19 * j + 19);
                    gra.DrawString(s + (char)(M[i][j] + 48), new Font("Microsoft Sans Serif", 8), Brushes.Black, 1 + 19 * i, 1 + 19 * j);
                }
            }
            closeMatrix.Top = Matrix.Height - 47;
            PlusVertex.Top = Matrix.Height - 47;
            MinusVertex.Top = Matrix.Height - 47;
            closeMatrix.Left = Matrix.Width - 30;
            PlusVertex.Left = 6;
            MinusVertex.Left = 30;
            if (E.Count() > V.Count())
            {
                for (int i = 0; i < (E.Count() - V.Count()); i++)
                {
                    Matrix.Width += 19;
                }
            }
            InMat.Image = new Bitmap(2000, 2000);
            Graphics grb = Graphics.FromImage(InMat.Image);
            grb.DrawString("", new Font("Microsoft Sans Serif", 8), Brushes.Black, 1, 1);
            grb.DrawLine(new Pen(Color.Black), 0, 19, 19, 19);
            grb.DrawLine(new Pen(Color.Black), 19, 0, 19, 19);
            grb.DrawLine(new Pen(Color.Black), 0, 0, 0, 19);
            grb.DrawLine(new Pen(Color.Black), 0, 0, 19, 0);
            InMat.Height = 20;
            InMat.Width = 20;
            for (int i = 1; i < E.Count() + 1; i++)
            {
                grb.DrawLine(new Pen(Color.Black), 19 * i + 0, 19, 19 * i + 19, 19);
                grb.DrawLine(new Pen(Color.Black), 19 * i + 19, 0, 19 * i + 19, 19);
                grb.DrawLine(new Pen(Color.Black), 19 * i + 0, 0, 19 * i + 19, 0);
                grb.DrawString(E[i - 1].num, new Font("Microsoft Sans Serif", 8), Brushes.Black, 1 + 19 * i, 1);
                InMat.Width += 19;
            }
            for (int i = 1; i < V.Count() + 1; i++)
            {
                grb.DrawLine(new Pen(Color.Black), 0, 19 * i + 19, 19, 19 * i + 19);
                grb.DrawLine(new Pen(Color.Black), 19, 19 * i + 0, 19, 19 * i + 19);
                grb.DrawLine(new Pen(Color.Black), 0, 19 * i + 0, 0, 19 * i + 19);
                grb.DrawString(V[i - 1].num, new Font("Microsoft Sans Serif", 8), Brushes.Black, 1, 1 + 19 * i);
                InMat.Height += 19;
            }
            for (int i = 1; i < E.Count() + 1; i++)
            {
                for (int j = 1; j < V.Count() + 1; j++)
                {
                    grb.DrawLine(new Pen(Color.Black), 19 * i + 0, 19 * j + 19, 19 * i + 19, 19 * j + 19);
                    grb.DrawLine(new Pen(Color.Black), 19 * i + 19, 19 * j + 0, 19 * i + 19, 19 * j + 19);
                    if ((j - 1 == E[i - 1].v2) || ((j - 1 == E[i - 1].v1) && (E[i - 1].Orient == false)))
                    {
                        grb.DrawString(" 1", new Font("Microsoft Sans Serif", 8), Brushes.Black, 1 + 19 * i, 1 + 19 * j);
                    }
                    else
                        if (j - 1 == E[i - 1].v1)
                    {
                        grb.DrawString("-1", new Font("Microsoft Sans Serif", 8), Brushes.Black, 1 + 19 * i, 1 + 19 * j);
                    }
                    else grb.DrawString(" 0", new Font("Microsoft Sans Serif", 8), Brushes.Black, 1 + 19 * i, 1 + 19 * j);
                }
            }
            closeMatrix2.Top = Matrix.Height - 47;
            closeMatrix2.Left = Matrix.Width - 30;
        }

        private void MoveVertex_Click(object sender, EventArgs e)
        {
            NewVertexButton.Enabled = true;
            Choose.Enabled = true;
            NewEdgeButton.Enabled = true;
            NewOEdgeButton.Enabled = true;
            DeleteVertex.Enabled = true;
            Loupe.Enabled = true;
            ShowMatrix.Enabled = true;
            MoveVertex.Enabled = false;
            Имя.Visible = false;
            ChangeName.Visible = false;
            NewName.Visible = false;
            VertexList.Visible = false;
            ColorBox.Visible = false;
            Selected_Name.Text = "Ничего не выбрано";
            choosen1 = -1;
        }

        private void sheet_MouseClick(object sender, MouseEventArgs e)
        {
            //TestMatrix.Visible = false;
            //нажата кнопка "Новая Вершина"
            if (NewVertexButton.Enabled == false)
            {
                V.Add(new Vertex(e.X, e.Y, num, MMM, LeftC, UpC));
                G.drawVertex(V[V.Count - 1], MMM, LeftC, UpC);
                num++;
                sheet.Image = G.GetBitmap();
                M[0].Add(M[0].Count);
                M.Add(new List<int>());
                M[M.Count() - 1].Add(M.Count() - 1);
                for (int i = 1; i < M.Count - 1; i++)
                {
                    M[i].Add(0);
                    M[M.Count - 1].Add(0);
                }
                M[M.Count - 1].Add(0);

            }

            //нажата кнопка "Перместить Вершину"
            if (MoveVertex.Enabled == false)
            {
                if (choosen1 != -1)
                {
                    V[choosen1].x = (e.X + LeftC) * MMM;
                    V[choosen1].y = (e.Y + UpC) * MMM;
                    G.clearSheet();
                    G.drawALLGraph(V, E, MMM, LeftC, UpC);
                    sheet.Image = G.GetBitmap();
                    choosen1 = -1;
                }
                else
                    for (int i = 0; i < V.Count; i++)
                    {
                        if (Math.Pow((V[i].x / MMM - e.X), 2) + Math.Pow((V[i].y / MMM - e.Y), 2) <= G.R * G.R)
                        {
                            G.drawSelectedVertex(V[i].x / MMM, V[i].y / MMM);
                            choosen1 = i;
                            sheet.Image = G.GetBitmap();
                            break;
                        }
                    }
            }

            //нажата кнопка "Экранная Лупа"
            if (Loupe.Enabled == false)
            {
                if (Mash > 1)
                {
                    if (e.X > 528)
                    {
                        LeftC = LeftC + 528 * MMM;
                    }
                    if (e.Y > 314)
                    {
                        UpC = UpC + 314 * MMM;
                    }

                    Mash = Mash - 1;
                    MMM = MMM / 2;
                    Mashtab.Text = "Масштаб " + 25 * MMM + "%";
                    G.clearSheet();
                    G.drawALLGraph(V, E, MMM, LeftC, UpC);
                    sheet.Image = G.GetBitmap();
                    Maximaze.Visible = true;
                }
                if (Mash == 1)
                {
                    Minimaze.Visible = false;
                }
            }

            //нажата кнопка "Выбрать"
            if (Choose.Enabled == false)
            {
                bool flag = false;
                Имя.Visible = true;
                ChangeName.Visible = true;
                NewName.Visible = true;
                //Вершина
                for (int i = 0; i < V.Count; i++)
                {
                    if (Math.Pow((V[i].x / MMM - e.X), 2) + Math.Pow((V[i].y / MMM - e.Y), 2) <= G.R * G.R)
                    {
                        VertexList.Visible = true;
                        VertexList.Items.Clear();
                        VertexList.Items.Add("Пути из вершины:");
                        List<int> Founded = new List<int>();
                        List<int> Prev = new List<int>();
                        for (int j = 0; j < V.Count(); j++)
                        {
                            if (j == i) Founded.Add(0);
                            else Founded.Add(-1);
                            Prev.Add(-1);
                        }
                        ColorBox.Visible = true;
                        Founded = Findways(Founded, i, Prev, 0, i);
                        for (int o = 0; o < Founded.Count(); o++)
                        {
                            if ((Founded[o] != -1) && (o != i))
                            {
                                int j = o;
                                String ss = "";
                                while (j != i)
                                {
                                    ss = " -> " + V[j].num + ss;
                                    j = Prev[j];
                                }
                                ss = V[i].num + ss;
                                VertexList.Items.Add(ss);
                            }
                        }
                        Founded.Clear();
                        flag = true;
                        string selname = "Вершина ";
                        selname += V[i].num;
                        Selected_Name.Text = selname;
                        NewName.Text = V[i].num;
                        if (choosen1 != -1)
                        {
                            choosen1 = -1;
                            G.clearSheet();
                            G.drawALLGraph(V, E, MMM, LeftC, UpC);
                            sheet.Image = G.GetBitmap();
                        }
                        if (choosen1 == -1)
                        {
                            G.drawSelectedVertex(V[i].x / MMM, V[i].y / MMM);
                            choosen1 = i;
                            sheet.Image = G.GetBitmap();
                            break;
                        }
                    }
                }
                if (!flag)
                {
                    //Ребро
                    for (int i = 0; i < E.Count; i++)
                    {
                        if (E[i].v1 == E[i].v2) //если это петля
                        {
                            if ((Math.Pow((V[E[i].v1].x / MMM - G.R - e.X), 2) + Math.Pow((V[E[i].v1].y / MMM - G.R - e.Y), 2) <= ((G.R + 2) * (G.R + 2))) &&
                                (Math.Pow((V[E[i].v1].x / MMM - G.R - e.X), 2) + Math.Pow((V[E[i].v1].y / MMM - G.R - e.Y), 2) >= ((G.R - 2) * (G.R - 2))))
                            {
                                string selname = "Ребро ";
                                selname += E[i].num;
                                Selected_Name.Text = selname;
                                NewName.Text = E[i].num;
                                VertexList.Visible = true;
                                VertexList.Items.Clear();
                                VertexList.Items.Add("Петля вершины " + V[E[i].v1].num);
                                if (choosenEd != -1)
                                {
                                    choosenEd = -1;
                                    G.clearSheet();
                                    G.drawALLGraph(V, E, MMM, LeftC, UpC);
                                    sheet.Image = G.GetBitmap();
                                }
                                if (choosenEd == -1)
                                {
                                    G.drawSelectedEdge(V[E[i].v1], V[E[i].v2], E[i], MMM, LeftC, UpC);
                                    choosenEd = i;
                                    sheet.Image = G.GetBitmap();
                                    flag = true;
                                    break;
                                }
                            }
                        }
                        else //не петля
                        {
                            if ((((e.X - V[E[i].v1].x / MMM) * (V[E[i].v2].y / MMM - V[E[i].v1].y / MMM) / (V[E[i].v2].x / MMM - V[E[i].v1].x / MMM) + V[E[i].v1].y / MMM) <= (e.Y + 4)) &&
                                (((e.X - V[E[i].v1].x / MMM) * (V[E[i].v2].y / MMM - V[E[i].v1].y / MMM) / (V[E[i].v2].x / MMM - V[E[i].v1].x / MMM) + V[E[i].v1].y / MMM) >= (e.Y - 4)))
                            {
                                if (((V[E[i].v1].x / MMM <= V[E[i].v2].x / MMM) && (V[E[i].v1].x / MMM <= e.X && e.X <= V[E[i].v2].x / MMM)) ||
                                    ((V[E[i].v1].x / MMM >= V[E[i].v2].x / MMM) && (V[E[i].v1].x / MMM >= e.X && e.X >= V[E[i].v2].x / MMM)))
                                {
                                    string selname = "Ребро ";
                                    selname += E[i].num;
                                    Selected_Name.Text = selname;
                                    NewName.Text = E[i].num;
                                    VertexList.Visible = true;
                                    VertexList.Items.Clear();
                                    VertexList.Items.Add("Ребро соединяет вершины ");
                                    VertexList.Items.Add(V[E[i].v1].num + " и " + V[E[i].v2].num);
                                    if (choosenEd != -1)
                                    {
                                        choosenEd = -1;
                                        G.clearSheet();
                                        G.drawALLGraph(V, E, MMM, LeftC, UpC);
                                        sheet.Image = G.GetBitmap();
                                    }
                                    if (choosenEd == -1)
                                    {
                                        G.drawSelectedEdge(V[E[i].v1], V[E[i].v2], E[i], MMM, LeftC, UpC);
                                        choosenEd = i;
                                        sheet.Image = G.GetBitmap();
                                        flag = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //нажата кнопка "Новое Ребро (Неориентированное)"
            if (NewEdgeButton.Enabled == false)
            {
                if (e.Button == MouseButtons.Left)
                {
                    for (int i = 0; i < V.Count; i++)
                    {
                        if (Math.Pow((V[i].x / MMM - e.X), 2) + Math.Pow((V[i].y / MMM - e.Y), 2) <= G.R * G.R)
                        {
                            if (choosen1 == -1)
                            {
                                G.drawSelectedVertex(V[i].x / MMM, V[i].y / MMM);
                                choosen1 = i;
                                sheet.Image = G.GetBitmap();
                                break;
                            }
                            if ((choosen2 == -1) && (M[choosen1 + 1][i + 1] == 0) && (M[i + 1][choosen1 + 1] == 0))
                            {
                                G.drawSelectedVertex(V[i].x / MMM, V[i].y / MMM);
                                choosen2 = i;
                                E.Add(new Edge(choosen1, choosen2, false, numE));
                                V[choosen1].contacts.Add(E.Count - 1);
                                V[choosen2].contacts.Add(E.Count - 1);
                                numE++;
                                G.drawEdge(V[choosen1], V[choosen2], E[E.Count - 1], E.Count - 1, MMM, LeftC, UpC);
                                M[choosen1 + 1][choosen2 + 1] = 1;
                                M[choosen2 + 1][choosen1 + 1] = 1;
                                choosen1 = -1;
                                choosen2 = -1;
                                sheet.Image = G.GetBitmap();
                                break;
                            }
                        }
                    }
                }
                if (e.Button == MouseButtons.Right)
                {
                    if ((choosen1 != -1) &&
                        (Math.Pow((V[choosen1].x / MMM - e.X), 2) + Math.Pow((V[choosen1].y / MMM - e.Y), 2) <= G.R * G.R))
                    {
                        G.drawVertex(V[choosen1], MMM, LeftC, UpC);
                        choosen1 = -1;
                        sheet.Image = G.GetBitmap();
                    }
                }
            }

            //нажата кнопка "Новое Ребро (Ориентированное)"
            if (NewOEdgeButton.Enabled == false)
            {
                if (e.Button == MouseButtons.Left)
                {
                    for (int i = 0; i < V.Count; i++)
                    {
                        if (Math.Pow((V[i].x / MMM - e.X), 2) + Math.Pow((V[i].y / MMM - e.Y), 2) <= G.R * G.R)
                        {
                            if (choosen1 == -1)
                            {
                                G.drawSelectedVertex(V[i].x / MMM, V[i].y / MMM);
                                choosen1 = i;
                                sheet.Image = G.GetBitmap();
                                break;
                            }
                            if ((choosen2 == -1) && (M[choosen1 + 1][i + 1] == 0))
                            {
                                G.drawSelectedVertex(V[i].x / MMM, V[i].y / MMM);
                                choosen2 = i;
                                E.Add(new Edge(choosen1, choosen2, true, numE));
                                numE++;
                                G.drawOEdge(V[choosen1], V[choosen2], E[E.Count - 1], E.Count - 1, MMM, LeftC, UpC);
                                V[choosen1].contacts.Add(E.Count - 1);
                                M[choosen1 + 1][choosen2 + 1] = 1;
                                choosen1 = -1;
                                choosen2 = -1;
                                sheet.Image = G.GetBitmap();
                                break;
                            }
                        }
                    }
                }
                if (e.Button == MouseButtons.Right)
                {
                    if ((choosen1 != -1) &&
                        (Math.Pow((V[choosen1].x / MMM - e.X), 2) + Math.Pow((V[choosen1].y / MMM - e.Y), 2) <= G.R * G.R))
                    {
                        G.drawVertex(V[choosen1], MMM, LeftC, UpC);
                        choosen1 = -1;
                        sheet.Image = G.GetBitmap();
                    }
                }
            }

            //нажата кнопка "удалить элемент"
            if (DeleteVertex.Enabled == false)
            {
                bool flag = false; //удалили ли что-нибудь по ЭТОМУ клику
                //ищем, возможно была нажата вершина
                for (int i = 0; i < V.Count; i++)
                {
                    if (Math.Pow((V[i].x / MMM - e.X), 2) + Math.Pow((V[i].y / MMM - e.Y), 2) <= G.R * G.R)
                    {
                        for (int j = 0; j < E.Count; j++)
                        {
                            if ((E[j].v1 == i) || (E[j].v2 == i))
                            {
                                E.RemoveAt(j);
                                for (int k = 0; k < V.Count; k++)
                                {
                                    for (int J = 0; J < V[k].contacts.Count; J++)
                                    {
                                        if (V[k].contacts[J] == j)
                                        {
                                            V[k].contacts.Remove(j);
                                        }
                                    }
                                }
                                j--;
                            }
                            else
                            {
                                if (E[j].v1 > i) E[j].v1--;
                                if (E[j].v2 > i) E[j].v2--;
                            }
                        }
                        V.RemoveAt(i);
                        for (int u = 1; u < M.Count(); u++)
                        {
                            for (int v = i + 1; v < M.Count() - 1; v++)
                            {
                                M[u][v] = M[u][v + 1];
                            }
                        }
                        for (int u = 1; u < M.Count(); u++)
                        {
                            for (int v = i + 1; v < M.Count() - 1; v++)
                            {
                                M[v][u] = M[u][v + 1];
                            }
                        }
                        for (int u = 0; u < M.Count(); u++)
                        {
                            M[u].RemoveAt(M.Count() - 1);
                        }
                        M.RemoveAt(M.Count() - 1);
                        flag = true;
                        break;
                    }
                }
                //ищем, возможно было нажато ребро
                if (!flag)
                {
                    for (int i = 0; i < E.Count; i++)
                    {
                        if (E[i].v1 == E[i].v2) //если это петля
                        {
                            if ((Math.Pow((V[E[i].v1].x / MMM - G.R - e.X), 2) + Math.Pow((V[E[i].v1].y / MMM - G.R - e.Y), 2) <= ((G.R + 2) * (G.R + 2))) &&
                                (Math.Pow((V[E[i].v1].x / MMM - G.R - e.X), 2) + Math.Pow((V[E[i].v1].y / MMM - G.R - e.Y), 2) >= ((G.R - 2) * (G.R - 2))))
                            {
                                for (int j = 0; j < V.Count; j++)
                                {
                                    for (int J = 0; J < V[j].contacts.Count; J++)
                                    {
                                        if (V[j].contacts[J] == i)
                                        {
                                            V[j].contacts.Remove(i);
                                        }
                                    }
                                }
                                M[E[i].v1 + 1][E[i].v1 + 1] = 0;
                                E.RemoveAt(i);
                                flag = true;
                                break;
                            }
                        }
                        else //не петля
                        {
                            bool wo = false;
                            if (((e.X - V[E[i].v1].x / MMM) * (V[E[i].v2].y / MMM - V[E[i].v1].y / MMM) / (V[E[i].v2].x / MMM - V[E[i].v1].x / MMM) + V[E[i].v1].y / MMM) <= (e.Y + 4) &&
                                ((e.X - V[E[i].v1].x / MMM) * (V[E[i].v2].y / MMM - V[E[i].v1].y / MMM) / (V[E[i].v2].x / MMM - V[E[i].v1].x / MMM) + V[E[i].v1].y / MMM) >= (e.Y - 4))
                            {
                                if ((V[E[i].v1].x / MMM <= V[E[i].v2].x / MMM && V[E[i].v1].x / MMM <= e.X && e.X <= V[E[i].v2].x / MMM) ||
                                    (V[E[i].v1].x / MMM >= V[E[i].v2].x / MMM && V[E[i].v1].x / MMM >= e.X && e.X >= V[E[i].v2].x / MMM))
                                {
                                    if (E[i].Orient == true)
                                    {
                                        wo = true;
                                    }
                                    for (int j = 0; j < V.Count; j++)
                                    {
                                        for (int J = 0; J < V[j].contacts.Count; J++)
                                        {
                                            if (V[j].contacts[J] == i)
                                            {
                                                V[j].contacts.Remove(i);
                                            }
                                        }
                                    }
                                    for (int j = 0; j < V.Count; j++)
                                    {
                                        for (int J = 0; J < V[j].contacts.Count; J++)
                                        {
                                            if (V[j].contacts[J] > i)
                                            {
                                                V[j].contacts[J]--;
                                            }
                                        }
                                    }
                                    M[E[i].v1 + 1][E[i].v2 + 1] = 0;
                                    if (!wo)
                                    {
                                        M[E[i].v2 + 1][E[i].v1 + 1] = 0;
                                    }
                                    E.RemoveAt(i);
                                    flag = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                //если что-то было удалено, то обновляем граф на экране
                if (flag)
                {
                    G.clearSheet();
                    G.drawALLGraph(V, E, MMM, LeftC, UpC);
                    sheet.Image = G.GetBitmap();
                }
            }
        }

        private void ChangeName_Click(object sender, EventArgs e)
        {
            //Для вершины
            if (Selected_Name.Text[0] == 'В')
            {
                if (NewName.Text.Count() < 6)
                {
                    V[choosen1].num = NewName.Text;
                    string selname = "Вершина ";
                    selname += NewName.Text;
                    Selected_Name.Text = selname;
                    G.clearSheet();
                    G.drawALLGraph(V, E, MMM, LeftC, UpC);
                    G.drawSelectedVertex(V[choosen1].x, V[choosen1].y);
                    sheet.Image = G.GetBitmap();
                }
            }
            //Для ребра
            if (Selected_Name.Text[0] == 'Р')
            {
                E[choosenEd].num = NewName.Text;
                string selname = "Ребро ";
                selname += NewName.Text;
                Selected_Name.Text = selname;
                G.clearSheet();
                G.drawALLGraph(V, E, MMM, LeftC, UpC);
                G.drawSelectedEdge(V[E[choosenEd].v1], V[E[choosenEd].v2], E[choosenEd], MMM, LeftC, UpC);
                sheet.Image = G.GetBitmap();
            }
        }

        private void Maximaze_Click(object sender, EventArgs e)
        {
            if (Mash > 1)
            {
                Mash = Mash - 1;
                MMM = MMM / 2;
                Mashtab.Text = "Масштаб " + 400 / MMM + "%";
                G.clearSheet();
                G.drawALLGraph(V, E, MMM, LeftC, UpC);
                sheet.Image = G.GetBitmap();
                Minimaze.Visible = true;
            }
            if (Mash == 1)
            {
                Maximaze.Visible = false;
            }
        }    //Тут бага

        private void Minimaze_Click(object sender, EventArgs e)
        {
            if (Mash < 6)
            {
                Mash = Mash + 1;
                MMM = MMM * 2;
                Mashtab.Text = "Масштаб " + 400 / MMM + "%";
                if (LeftC > 0)
                {
                    LeftC = LeftC - 528 * MMM;
                }
                if (UpC > 0)
                {
                    UpC = UpC - 314 * MMM;
                }
                G.clearSheet();
                G.drawALLGraph(V, E, MMM, LeftC, UpC);
                sheet.Image = G.GetBitmap();
                Maximaze.Visible = true;
            }
            if (Mash == 6)
            {
                Minimaze.Visible = false;
            }
           
        }    //Тут бага

        private void closeMatrix_Click(object sender, EventArgs e)
        {
            Matrix.Visible = false;
            ShowMatrix.Enabled = true;
        }

        private void closeMatrix2_Click(object sender, EventArgs e)
        {
            Matrix.Visible = false;
            ShowMatrix.Enabled = true;
        }

        private void ColorBox1_Click(object sender, EventArgs e)
        {
            V[choosen1].color = Brushes.White;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        private void ColorBox2_Click(object sender, EventArgs e)
        {
            V[choosen1].color = Brushes.LightGray;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        private void ColorBox3_Click(object sender, EventArgs e)
        {
            V[choosen1].color = Brushes.DarkGray;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        private void ColorBox4_Click(object sender, EventArgs e)
        {
            V[choosen1].color = Brushes.Gray;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        private void ColorBox5_Click(object sender, EventArgs e)
        {
            V[choosen1].color = Brushes.Black;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        private void ColorBox6_Click(object sender, EventArgs e)
        {
            V[choosen1].color = Brushes.Red;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        private void ColorBox7_Click(object sender, EventArgs e)
        {
            V[choosen1].color = Brushes.Orange;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        private void ColorBox8_Click(object sender, EventArgs e)
        {
            V[choosen1].color = Brushes.Yellow;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        private void ColorBox9_Click(object sender, EventArgs e)
        {
            V[choosen1].color = Brushes.Lime;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        private void ColorBox10_Click(object sender, EventArgs e)
        {
            V[choosen1].color = Brushes.LimeGreen;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        private void ColorBox11_Click(object sender, EventArgs e)
        {
            V[choosen1].color = Brushes.Indigo;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        private void ColorBox12_Click(object sender, EventArgs e)
        {
            V[choosen1].color = Brushes.Blue;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        private void ColorBox13_Click(object sender, EventArgs e)
        {
            V[choosen1].color = Brushes.LightBlue;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        private void ColorBox14_Click(object sender, EventArgs e)
        {
            V[choosen1].color = Brushes.Aquamarine;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        private void ColorBox15_Click(object sender, EventArgs e)
        {
            V[choosen1].color = Brushes.Green;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        private void ColorBox16_Click(object sender, EventArgs e)
        {
            V[choosen1].color = Brushes.Purple;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        private void ColorBox17_Click(object sender, EventArgs e)
        {
            V[choosen1].color = Brushes.Violet;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        private void ColorBox18_Click(object sender, EventArgs e)
        {
            V[choosen1].color = Brushes.Pink;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        private void ColorBox19_Click(object sender, EventArgs e)
        {
            V[choosen1].color = Brushes.DarkRed;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        private void ColorBox20_Click(object sender, EventArgs e)
        {
            V[choosen1].color = Brushes.Brown;
            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        public int less_Color (List<Brush> cols, int num)
        {
            int col = -1;
            int flag = 0;
            while (flag == 0)
            {
                col++;
                flag = 1;
                for (int i = 0; i < E.Count; i++)
                {
                    Edge e = E[i];
                    if ((e.v1 != num) && (e.v2 == num))
                    {
                        if (V[e.v1].color == cols[col])
                        {
                            flag = 0;
                        }
                    }
                    if ((e.v2 != num) && (e.v1 == num))
                    {
                        if (V[e.v2].color == cols[col])
                        {
                            flag = 0;
                        }
                    }
                }
            }
            return col;
        }

        private void Painting_Click(object sender, EventArgs e)
        {
            List<Brush> Colors = new List<Brush>();
            Colors.Add(Brushes.Red);
            Colors.Add(Brushes.Blue);
            Colors.Add(Brushes.Yellow);
            Colors.Add(Brushes.LimeGreen);
            Colors.Add(Brushes.AliceBlue);
            Colors.Add(Brushes.AntiqueWhite);
            Colors.Add(Brushes.Aqua);
            Colors.Add(Brushes.Aquamarine);
            Colors.Add(Brushes.Azure);
            Colors.Add(Brushes.Beige);
            Colors.Add(Brushes.Bisque);
            Colors.Add(Brushes.Black);
            Colors.Add(Brushes.BlanchedAlmond);
            Colors.Add(Brushes.BlueViolet);
            Colors.Add(Brushes.Brown);
            Colors.Add(Brushes.BurlyWood);
            Colors.Add(Brushes.CadetBlue);
            Colors.Add(Brushes.Chartreuse);
            Colors.Add(Brushes.Chocolate);
            Colors.Add(Brushes.Coral);
            Colors.Add(Brushes.CornflowerBlue);
            Colors.Add(Brushes.Cornsilk);
            Colors.Add(Brushes.Crimson);
            Colors.Add(Brushes.Cyan);
            Colors.Add(Brushes.DarkBlue);
            Colors.Add(Brushes.DarkCyan);
            Colors.Add(Brushes.DarkGoldenrod);
            Colors.Add(Brushes.DarkGray);
            Colors.Add(Brushes.DarkGreen);
            Colors.Add(Brushes.DarkKhaki);
            Colors.Add(Brushes.DarkMagenta);
            Colors.Add(Brushes.DarkOliveGreen);
            Colors.Add(Brushes.DarkOrange);
            Colors.Add(Brushes.DarkOrchid);
            Colors.Add(Brushes.DarkRed);
            Colors.Add(Brushes.DarkSalmon);
            Colors.Add(Brushes.DarkSeaGreen);
            Colors.Add(Brushes.DarkSlateBlue);
            for (int i=0; i<V.Count; i++)
            {
                V[i].color = Brushes.White;
            }
            for (int i = 0; i < V.Count; i++)
            {
                if (less_Color(Colors, i) != -1)
                {
                    V[i].color = Colors[less_Color(Colors, i)];
                }
                else
                {
                    break;
                }
            }
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        public List<int> cliq_for_seq (List<int> sequence)
        {
            List<int> finalseq = sequence;
            for (int i=0; i<V[sequence[0]].contacts.Count; i++)
            {
                Edge e = E[V[sequence[0]].contacts[i]];
                int candidate = -1;
                if ((e.v1 == sequence[0]) && (e.v2 != sequence[0]))
                {
                    candidate = e.v2;
                }
                if ((e.v1 != sequence[0]) && (e.v2 == sequence[0]))
                {
                    candidate = e.v1;
                }
                if ((candidate!=-1) && (!sequence.Exists(element => element == candidate)))
                {
                    int flag = 1;
                    for (int j=1; j<sequence.Count; j++)
                    {
                        int f = 0;
                        for (int kek=0; kek<V[sequence[j]].contacts.Count; kek++)
                        {
                            if ((E[V[sequence[j]].contacts[kek]].v1 == candidate) || (E[V[sequence[j]].contacts[kek]].v2 == candidate))
                            {
                                f = 1;
                            }
                        }
                        if (f==0)
                        {
                            flag = 0;
                        }
                    }
                    if (flag == 1)
                    {
                        List<int> newSeq = sequence;
                        newSeq.Add(candidate);
                        newSeq = cliq_for_seq(newSeq);
                        if (newSeq.Count > finalseq.Count)
                        {
                            finalseq = newSeq;
                        }
                    }
                }
            }
            return finalseq;
        }

        private void Clique_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < V.Count; i++)
            {
                V[i].color = Brushes.White;
            }
            List<int> ourseq = new List<int>();
            ourseq.Add(0);
            for (int i=0; i<V.Count; i++)
            {
                List<int> curseq = new List<int>();
                curseq.Add(i);
                curseq = cliq_for_seq(curseq);
                if (curseq.Count > ourseq.Count)
                {
                    ourseq = curseq;
                }
            }
            for (int i=0; i<ourseq.Count; i++)
            {
                V[ourseq[i]].color = Brushes.Red;
            }
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
        }

        private void SaveGraph_Click(object sender, EventArgs e)
        {
            if (currentFolder == "")
            {
                SaveFileDialog SaveFile = new SaveFileDialog();
                SaveFile.Title = "Выберите путь для сохранения файла";
                if (SaveFile.ShowDialog() == DialogResult.OK)
                {
                    currentFolder = SaveFile.FileName;
                    StreamWriter writer = new StreamWriter(SaveFile.OpenFile());
                    String S = "p edge " + (V.Count()).ToString() + " " + (E.Count()).ToString();
                    writer.WriteLine(S);
                    for (int i=0; i<E.Count(); i++)
                    {
                        writer.WriteLine("e " + (E[i].v1+1).ToString() + " " + (E[i].v2 + 1).ToString());
                    }
                    writer.Dispose();
                    writer.Close();
                }
            } else
            {
                StreamWriter writer = new StreamWriter(currentFolder);
                String S = "p edge " + (V.Count()).ToString() + " " + (E.Count()).ToString();
                writer.WriteLine(S);
                for (int i = 0; i < E.Count(); i++)
                {
                    writer.WriteLine("e " + (E[i].v1 + 1).ToString() + " " + (E[i].v2 + 1).ToString());
                }
                writer.Dispose();
                writer.Close();
            }
        }

        private void LoadGraph_Click(object sender, EventArgs e)
        {
            V = new List<Vertex>();
            G = new DrawGraph(sheet.Width, sheet.Height);
            E = new List<Edge>();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
            OpenFileDialog OpenFile = new OpenFileDialog();
            OpenFile.Title = "Выберите файл с сохранённым графом";
            if (OpenFile.ShowDialog() == DialogResult.OK)
            {
                currentFolder = OpenFile.FileName;
                StreamReader reader = new StreamReader(currentFolder);
                String S = reader.ReadLine();
                int Vnum = 0;
                String VN = "";
                String EN = "";
                int Enum = 0;
                int i = 0;
                while (S[i] != ' ') i++;
                i++;
                while (S[i] != ' ') i++;
                i++;
                while (S[i]!=' ')
                {
                    VN = VN + S[i];
                    i++;
                }
                i++;
                while (i<S.Length)
                {
                    EN = EN + S[i];
                    i++;
                }
                Vnum = int.Parse(VN);
                Enum = int.Parse(EN);
                Random Rnd = new Random();
                for (i=0; i<Vnum; i++)
                {
                    V.Add(new Vertex(Rnd.Next(800), Rnd.Next(400), num, MMM, LeftC, UpC));
                    num++;
                    G.drawALLGraph(V, E, MMM, LeftC, UpC);
                }
                sheet.Image = G.GetBitmap();
                for (int j=0; j<Enum; j++)
                {
                    i = 0;
                    S = reader.ReadLine();
                    while (S[i] != ' ') i++;
                    i++;
                    int l, r;
                    string s = "";
                    while (S[i] != ' ')
                    {
                        s = s + S[i];
                        i++;
                    }
                    i++;
                    l = int.Parse(s);
                    s = "";
                    while (i < S.Length)
                    {
                        s = s + S[i];
                        i++;
                    }
                    r = int.Parse(s);
                    E.Add(new Edge(l-1, r-1, true, E.Count()));
                    G.drawALLGraph(V, E, MMM, LeftC, UpC);
                    sheet.Image = G.GetBitmap();
                }
            }
        }

        private void PlusVertex_Click(object sender, EventArgs e)
        {
            Random Rnd = new Random();
            V.Add(new Vertex(Rnd.Next(800), Rnd.Next(400), num, MMM, LeftC, UpC));
            G.drawVertex(V[V.Count - 1], MMM, LeftC, UpC);
            num++;
            sheet.Image = G.GetBitmap();
            M[0].Add(M[0].Count);
            M.Add(new List<int>());
            M[M.Count() - 1].Add(M.Count() - 1);
            for (int i = 1; i < M.Count - 1; i++)
            {
                M[i].Add(0);
                M[M.Count - 1].Add(0);
            }
            M[M.Count - 1].Add(0);

            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            NewVertexButton.Enabled = true;
            Choose.Enabled = true;
            NewEdgeButton.Enabled = true;
            NewOEdgeButton.Enabled = true;
            DeleteVertex.Enabled = true;
            Loupe.Enabled = true;
            ShowMatrix.Enabled = false;
            MoveVertex.Enabled = true;
            Matrix.Visible = true;
            AdMat.Image = new Bitmap(2000, 2000);
            Graphics gra = Graphics.FromImage(AdMat.Image);
            gra.DrawString("", new Font("Microsoft Sans Serif", 8), Brushes.Black, 1, 1);
            gra.DrawLine(new Pen(Color.Black), 0, 19, 19, 19);
            gra.DrawLine(new Pen(Color.Black), 19, 0, 19, 19);
            gra.DrawLine(new Pen(Color.Black), 0, 0, 0, 19);
            gra.DrawLine(new Pen(Color.Black), 0, 0, 19, 0);
            Matrix.Height = 94;
            Matrix.Width = 250;
            AdMat.Height = 20;
            AdMat.Width = 20;
            for (int i = 1; i < M.Count(); i++)
            {
                gra.DrawLine(new Pen(Color.Black), 19 * i + 0, 19, 19 * i + 19, 19);
                gra.DrawLine(new Pen(Color.Black), 19 * i + 19, 0, 19 * i + 19, 19);
                gra.DrawLine(new Pen(Color.Black), 19 * i + 0, 0, 19 * i + 19, 0);
                gra.DrawString(V[M[0][i] - 1].num, new Font("Microsoft Sans Serif", 8), Brushes.Black, 1 + 19 * i, 1);
                AdMat.Width += 19;
                if (i > 12)
                {
                    Matrix.Width += 19;
                }
                gra.DrawLine(new Pen(Color.Black), 0, 19 * i + 19, 19, 19 * i + 19);
                gra.DrawLine(new Pen(Color.Black), 19, 19 * i + 0, 19, 19 * i + 19);
                gra.DrawLine(new Pen(Color.Black), 0, 19 * i + 0, 0, 19 * i + 19);
                gra.DrawString(V[M[0][i] - 1].num, new Font("Microsoft Sans Serif", 8), Brushes.Black, 1, 1 + 19 * i);
                AdMat.Height += 19;
                Matrix.Height += 19;
            }
            for (int i = 1; i < M.Count(); i++)
            {
                for (int j = 1; j < M[i].Count(); j++)
                {
                    string s = " ";
                    gra.DrawLine(new Pen(Color.Black), 19 * i + 0, 19 * j + 19, 19 * i + 19, 19 * j + 19);
                    gra.DrawLine(new Pen(Color.Black), 19 * i + 19, 19 * j + 0, 19 * i + 19, 19 * j + 19);
                    gra.DrawString(s + (char)(M[i][j] + 48), new Font("Microsoft Sans Serif", 8), Brushes.Black, 1 + 19 * i, 1 + 19 * j);
                }
            }
            closeMatrix.Top = Matrix.Height - 47;
            PlusVertex.Top = Matrix.Height - 47;
            MinusVertex.Top = Matrix.Height - 47;
            closeMatrix.Left = Matrix.Width - 30;
            PlusVertex.Left = 6;
            MinusVertex.Left = 30;
            if (E.Count() > V.Count())
            {
                for (int i = 0; i < (E.Count() - V.Count()); i++)
                {
                    Matrix.Width += 19;
                }
            }
            InMat.Image = new Bitmap(2000, 2000);
            Graphics grb = Graphics.FromImage(InMat.Image);
            grb.DrawString("", new Font("Microsoft Sans Serif", 8), Brushes.Black, 1, 1);
            grb.DrawLine(new Pen(Color.Black), 0, 19, 19, 19);
            grb.DrawLine(new Pen(Color.Black), 19, 0, 19, 19);
            grb.DrawLine(new Pen(Color.Black), 0, 0, 0, 19);
            grb.DrawLine(new Pen(Color.Black), 0, 0, 19, 0);
            InMat.Height = 20;
            InMat.Width = 20;
            for (int i = 1; i < E.Count() + 1; i++)
            {
                grb.DrawLine(new Pen(Color.Black), 19 * i + 0, 19, 19 * i + 19, 19);
                grb.DrawLine(new Pen(Color.Black), 19 * i + 19, 0, 19 * i + 19, 19);
                grb.DrawLine(new Pen(Color.Black), 19 * i + 0, 0, 19 * i + 19, 0);
                grb.DrawString(E[i - 1].num, new Font("Microsoft Sans Serif", 8), Brushes.Black, 1 + 19 * i, 1);
                InMat.Width += 19;
            }
            for (int i = 1; i < V.Count() + 1; i++)
            {
                grb.DrawLine(new Pen(Color.Black), 0, 19 * i + 19, 19, 19 * i + 19);
                grb.DrawLine(new Pen(Color.Black), 19, 19 * i + 0, 19, 19 * i + 19);
                grb.DrawLine(new Pen(Color.Black), 0, 19 * i + 0, 0, 19 * i + 19);
                grb.DrawString(V[i - 1].num, new Font("Microsoft Sans Serif", 8), Brushes.Black, 1, 1 + 19 * i);
                InMat.Height += 19;
            }
            for (int i = 1; i < E.Count() + 1; i++)
            {
                for (int j = 1; j < V.Count() + 1; j++)
                {
                    grb.DrawLine(new Pen(Color.Black), 19 * i + 0, 19 * j + 19, 19 * i + 19, 19 * j + 19);
                    grb.DrawLine(new Pen(Color.Black), 19 * i + 19, 19 * j + 0, 19 * i + 19, 19 * j + 19);
                    if ((j - 1 == E[i - 1].v2) || ((j - 1 == E[i - 1].v1) && (E[i - 1].Orient == false)))
                    {
                        grb.DrawString(" 1", new Font("Microsoft Sans Serif", 8), Brushes.Black, 1 + 19 * i, 1 + 19 * j);
                    }
                    else
                        if (j - 1 == E[i - 1].v1)
                    {
                        grb.DrawString("-1", new Font("Microsoft Sans Serif", 8), Brushes.Black, 1 + 19 * i, 1 + 19 * j);
                    }
                    else grb.DrawString(" 0", new Font("Microsoft Sans Serif", 8), Brushes.Black, 1 + 19 * i, 1 + 19 * j);
                }
            }
            closeMatrix2.Top = Matrix.Height - 47;
            closeMatrix2.Left = Matrix.Width - 30;
        }

        private void MinusVertex_Click(object sender, EventArgs e)
        {
            { int i = V.Count() - 1;
            for (int j = 0; j < E.Count; j++)
            {
                if ((E[j].v1 == i) || (E[j].v2 == i))
                {
                    E.RemoveAt(j);
                    for (int k = 0; k < V.Count; k++)
                    {
                        for (int J = 0; J < V[k].contacts.Count; J++)
                        {
                            if (V[k].contacts[J] == j)
                            {
                                V[k].contacts.Remove(j);
                            }
                        }
                    }
                    j--;
                }
                else
                {
                    if (E[j].v1 > i) E[j].v1--;
                    if (E[j].v2 > i) E[j].v2--;
                }
            }
            V.RemoveAt(i);
            for (int u = 1; u < M.Count(); u++)
            {
                for (int v = i + 1; v < M.Count() - 1; v++)
                {
                    M[u][v] = M[u][v + 1];
                }
            }
            for (int u = 1; u < M.Count(); u++)
            {
                for (int v = i + 1; v < M.Count() - 1; v++)
                {
                    M[v][u] = M[u][v + 1];
                }
            }
            for (int u = 0; u < M.Count(); u++)
            {
                M[u].RemoveAt(M.Count() - 1);
            }
            M.RemoveAt(M.Count() - 1); }

            G.clearSheet();
            G.drawALLGraph(V, E, MMM, LeftC, UpC);
            sheet.Image = G.GetBitmap();
            NewVertexButton.Enabled = true;
            Choose.Enabled = true;
            NewEdgeButton.Enabled = true;
            NewOEdgeButton.Enabled = true;
            DeleteVertex.Enabled = true;
            Loupe.Enabled = true;
            ShowMatrix.Enabled = false;
            MoveVertex.Enabled = true;
            Matrix.Visible = true;
            AdMat.Image = new Bitmap(2000, 2000);
            Graphics gra = Graphics.FromImage(AdMat.Image);
            gra.DrawString("", new Font("Microsoft Sans Serif", 8), Brushes.Black, 1, 1);
            gra.DrawLine(new Pen(Color.Black), 0, 19, 19, 19);
            gra.DrawLine(new Pen(Color.Black), 19, 0, 19, 19);
            gra.DrawLine(new Pen(Color.Black), 0, 0, 0, 19);
            gra.DrawLine(new Pen(Color.Black), 0, 0, 19, 0);
            Matrix.Height = 94;
            Matrix.Width = 250;
            AdMat.Height = 20;
            AdMat.Width = 20;
            for (int i = 1; i < M.Count(); i++)
            {
                gra.DrawLine(new Pen(Color.Black), 19 * i + 0, 19, 19 * i + 19, 19);
                gra.DrawLine(new Pen(Color.Black), 19 * i + 19, 0, 19 * i + 19, 19);
                gra.DrawLine(new Pen(Color.Black), 19 * i + 0, 0, 19 * i + 19, 0);
                gra.DrawString(V[M[0][i] - 1].num, new Font("Microsoft Sans Serif", 8), Brushes.Black, 1 + 19 * i, 1);
                AdMat.Width += 19;
                if (i > 12)
                {
                    Matrix.Width += 19;
                }
                gra.DrawLine(new Pen(Color.Black), 0, 19 * i + 19, 19, 19 * i + 19);
                gra.DrawLine(new Pen(Color.Black), 19, 19 * i + 0, 19, 19 * i + 19);
                gra.DrawLine(new Pen(Color.Black), 0, 19 * i + 0, 0, 19 * i + 19);
                gra.DrawString(V[M[0][i] - 1].num, new Font("Microsoft Sans Serif", 8), Brushes.Black, 1, 1 + 19 * i);
                AdMat.Height += 19;
                Matrix.Height += 19;
            }
            for (int i = 1; i < M.Count(); i++)
            {
                for (int j = 1; j < M[i].Count(); j++)
                {
                    string s = " ";
                    gra.DrawLine(new Pen(Color.Black), 19 * i + 0, 19 * j + 19, 19 * i + 19, 19 * j + 19);
                    gra.DrawLine(new Pen(Color.Black), 19 * i + 19, 19 * j + 0, 19 * i + 19, 19 * j + 19);
                    gra.DrawString(s + (char)(M[i][j] + 48), new Font("Microsoft Sans Serif", 8), Brushes.Black, 1 + 19 * i, 1 + 19 * j);
                }
            }
            closeMatrix.Top = Matrix.Height - 47;
            PlusVertex.Top = Matrix.Height - 47;
            MinusVertex.Top = Matrix.Height - 47;
            closeMatrix.Left = Matrix.Width - 30;
            PlusVertex.Left = 6;
            MinusVertex.Left = 30;
            if (E.Count() > V.Count())
            {
                for (int i = 0; i < (E.Count() - V.Count()); i++)
                {
                    Matrix.Width += 19;
                }
            }
            InMat.Image = new Bitmap(2000, 2000);
            Graphics grb = Graphics.FromImage(InMat.Image);
            grb.DrawString("", new Font("Microsoft Sans Serif", 8), Brushes.Black, 1, 1);
            grb.DrawLine(new Pen(Color.Black), 0, 19, 19, 19);
            grb.DrawLine(new Pen(Color.Black), 19, 0, 19, 19);
            grb.DrawLine(new Pen(Color.Black), 0, 0, 0, 19);
            grb.DrawLine(new Pen(Color.Black), 0, 0, 19, 0);
            InMat.Height = 20;
            InMat.Width = 20;
            for (int i = 1; i < E.Count() + 1; i++)
            {
                grb.DrawLine(new Pen(Color.Black), 19 * i + 0, 19, 19 * i + 19, 19);
                grb.DrawLine(new Pen(Color.Black), 19 * i + 19, 0, 19 * i + 19, 19);
                grb.DrawLine(new Pen(Color.Black), 19 * i + 0, 0, 19 * i + 19, 0);
                grb.DrawString(E[i - 1].num, new Font("Microsoft Sans Serif", 8), Brushes.Black, 1 + 19 * i, 1);
                InMat.Width += 19;
            }
            for (int i = 1; i < V.Count() + 1; i++)
            {
                grb.DrawLine(new Pen(Color.Black), 0, 19 * i + 19, 19, 19 * i + 19);
                grb.DrawLine(new Pen(Color.Black), 19, 19 * i + 0, 19, 19 * i + 19);
                grb.DrawLine(new Pen(Color.Black), 0, 19 * i + 0, 0, 19 * i + 19);
                grb.DrawString(V[i - 1].num, new Font("Microsoft Sans Serif", 8), Brushes.Black, 1, 1 + 19 * i);
                InMat.Height += 19;
            }
            for (int i = 1; i < E.Count() + 1; i++)
            {
                for (int j = 1; j < V.Count() + 1; j++)
                {
                    grb.DrawLine(new Pen(Color.Black), 19 * i + 0, 19 * j + 19, 19 * i + 19, 19 * j + 19);
                    grb.DrawLine(new Pen(Color.Black), 19 * i + 19, 19 * j + 0, 19 * i + 19, 19 * j + 19);
                    if ((j - 1 == E[i - 1].v2) || ((j - 1 == E[i - 1].v1) && (E[i - 1].Orient == false)))
                    {
                        grb.DrawString(" 1", new Font("Microsoft Sans Serif", 8), Brushes.Black, 1 + 19 * i, 1 + 19 * j);
                    }
                    else
                        if (j - 1 == E[i - 1].v1)
                    {
                        grb.DrawString("-1", new Font("Microsoft Sans Serif", 8), Brushes.Black, 1 + 19 * i, 1 + 19 * j);
                    }
                    else grb.DrawString(" 0", new Font("Microsoft Sans Serif", 8), Brushes.Black, 1 + 19 * i, 1 + 19 * j);
                }
            }
            closeMatrix2.Top = Matrix.Height - 47;
            closeMatrix2.Left = Matrix.Width - 30;
        }

        private void PrintButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog SaveFile = new SaveFileDialog();
            SaveFile.Title = "Выберите путь для сохранения файла";
            if (SaveFile.ShowDialog() == DialogResult.OK)
            {
                sheet.Image.Save(SaveFile.FileName, System.Drawing.Imaging.ImageFormat.Png);
            }
        }
    }
}
